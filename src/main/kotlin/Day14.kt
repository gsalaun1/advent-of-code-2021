import java.io.InputStream

fun main() {
    val day14 = Day14()
    existingTags.forEach { tag ->
        day14.run("day14-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 14 Part 1 : ${it.first.part1}")
            println("[$tag] Day 14 Part 2 : ${it.first.part2}")
        }
    }
}

class Day14 : Day<String, Long, Long>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Long {
        val polymerizer = Polymerizer(lines)
        return polymerizer.differenceBetweendMostAndLeastCommonElements(10)
    }

    override fun runPart2(lines: List<String>): Long {
        val polymerizer = Polymerizer(lines)
        return polymerizer.differenceBetweendMostAndLeastCommonElements(40)
    }
}

class Polymerizer(lines: List<String>) {
    private val template = Template(lines.first())

    private var rulesDictionary: Map<String, Pair<String, String>>

    init {
        val mutableRulesDictionary = mutableMapOf<String, Pair<String, String>>()
        lines
            .filter { it.contains("->") }
            .forEach { line ->
                val rule = line.split("->")
                val pattern = rule[0].trim()
                val leftPattern = pattern[0] + rule[1].trim()
                val rightPattern = rule[1].trim() + pattern[1]
                mutableRulesDictionary[pattern] = Pair(leftPattern, rightPattern)
            }
        rulesDictionary = mutableRulesDictionary.toMap()
    }

    fun applyRules(iterations: Int = 1): Template {
        return (1..iterations).fold(template) { acc, _ ->
            acc.apply(rulesDictionary)
        }
    }

    fun differenceBetweendMostAndLeastCommonElements(iterations: Int): Long {
        val generatedTemplate = applyRules(iterations)
        val charsCount = generatedTemplate.countChars()
        val mostCommonElementCount = charsCount.maxOf { it.value }
        val leastCommonElementCount = charsCount.minOf { it.value }
        return mostCommonElementCount - leastCommonElementCount
    }

    class Template {

        var patterns: Map<String, Long>

        private var lastChar: Char

        constructor(value: String) {
            val patternsBuilder = mutableMapOf<String, Long>()
            (0 until value.length - 1).forEach { index ->
                val currentPattern = "" + value[index] + value[index + 1]
                patternsBuilder[currentPattern] = patternsBuilder.getOrDefault(currentPattern, 0) + 1
            }
            patterns = patternsBuilder.toMap()
            lastChar = value.last()
        }

        constructor(patterns: Map<String, Long>, lastChar: Char) {
            this.patterns = patterns
            this.lastChar = lastChar
        }

        fun apply(rulesDictionary: Map<String, Pair<String, String>>): Template {
            val result = mutableMapOf<String, Long>()
            this.patterns.forEach { (pattern, count) ->
                rulesDictionary[pattern]?.let { generatedPatterns ->
                    result[generatedPatterns.first] = count + result.getOrDefault(generatedPatterns.first, 0)
                    result[generatedPatterns.second] = count + result.getOrDefault(generatedPatterns.second, 0)
                } ?: run {
                    result[pattern] = count
                }
            }
            return Template(result, lastChar)
        }

        fun countChars(): Map<Char, Long> {
            val result = mutableMapOf(lastChar to 1L)
            this.patterns.forEach { (pattern, count) ->
                result[pattern[0]] = result.getOrDefault(pattern[0], 0) + count
            }
            return result.toMap()
        }
    }
}
