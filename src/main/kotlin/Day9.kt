import java.io.InputStream

fun main() {
    val day9 = Day9()
    existingTags.forEach { tag ->
        day9.run("day9-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 9 Part 1 : ${it.first.part1}")
            println("[$tag] Day 9 Part 2 : ${it.first.part2}")
        }
    }
}

class Day9 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val smokeMatrix = SmokeMatrix(lines)
        return smokeMatrix.countRiskLevelsSum()
    }

    override fun runPart2(lines: List<String>): Int {
        val smokeMatrix = SmokeMatrix(lines)
        return smokeMatrix.multiplyLargestBasins()
    }
}

class SmokeMatrix() {

    private lateinit var points: List<SmokePoint>

    constructor(lines: List<String>) : this() {
        points = lines.flatMapIndexed { row, line ->
            line.toCharArray().mapIndexed { column, value ->
                SmokePoint(column, row, value.toString().toInt())
            }
        }
    }

    fun getLowPoints(): List<SmokePoint> {
        val result = mutableListOf<SmokePoint>()
        val idxLastRow = points.maxOf { it.y }
        val idxLastColumn = points.maxOf { it.x }
        (0..idxLastRow).forEach { idxRow ->
            (0..idxLastColumn).forEach { idxColumn ->
                val currentPoint = points.first { it.x == idxColumn && it.y == idxRow }
                val neighbors = findNeighborsOf(currentPoint)
                if (neighbors.all { it.value > currentPoint.value }) {
                    result.add(currentPoint)
                }
            }
        }
        return result
    }

    fun countRiskLevelsSum(): Int {
        return getLowPoints().sumOf { it.value + 1 }
    }

    fun findNeighborsOf(point: SmokePoint) = points.filter { it.x == point.x && it.y == point.y - 1 } +
        points.filter { it.x == point.x - 1 && it.y == point.y } +
        points.filter { it.x == point.x + 1 && it.y == point.y } +
        points.filter { it.x == point.x && it.y == point.y + 1 }

    fun getBasinPoints(lowPoint: SmokePoint, alreadyInBasin: MutableSet<SmokePoint> = mutableSetOf()): Set<SmokePoint> {
        val neighborsInBasin = findNeighborsOf(lowPoint)
            .filter { it.value < 9 }
            .filter { !alreadyInBasin.contains(it) }
            .toSet()
        alreadyInBasin.add(lowPoint)
        alreadyInBasin.addAll(neighborsInBasin)
        return neighborsInBasin + lowPoint + neighborsInBasin.flatMap { getBasinPoints(it, alreadyInBasin) }
    }

    fun getBasinsSize(): List<Int> {
        val lowPoints = getLowPoints()
        return lowPoints
            .map { getBasinPoints(it) }
            .map { it.size }
    }

    fun multiplyLargestBasins() = getBasinsSize().sorted().takeLast(3).reduce { acc, value ->
        acc * value
    }

    data class SmokePoint(val x: Int, val y: Int, val value: Int)
}
