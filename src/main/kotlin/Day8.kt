import java.io.InputStream

fun main() {
    val day8 = Day8()
    existingTags.forEach { tag ->
        day8.run("day8-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 8 Part 1 : ${it.first.part1}")
            println("[$tag] Day 8 Part 2 : ${it.first.part2}")
        }
    }
}

class Day8 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) = countEasyDigits(lines)

    override fun runPart2(lines: List<String>) =
        lines.sumOf {
            val signalAnalyzer = SignalAnalyzer(it)
            signalAnalyzer.decode()
        }
}

fun countEasyDigits(lines: List<String>): Int {
    val sortedLines = lines
        .map { it.split("|")[1] }
        .flatMap { output ->
            output.split(" ").filter {
                it.length == 2 || it.length == 4 || it.length == 3 || it.length == 7
            }
        }
    return sortedLines.size
}

class SignalAnalyzer(val line: String) {

    private var remainingSignal: MutableList<String>
    private var output: List<String>

    init {
        val splittedLine = line.split("|")
        remainingSignal = splittedLine[0].split(" ").toMutableList()
        output = splittedLine[1]
            .split(" ")
            .filter { it.isNotEmpty() }
            .map { it.sortAlphabetically() }
    }

    val numbers = mutableMapOf<String, Int>()

    fun analyzeSignal(): Map<String, Int> {
        val resultOne = stepOne_discoverOne()
        val resultSeven = stepTwo_discoverSeven(resultOne)
        val resultThree = stepThree_discoverThree(resultSeven)
        val resultZero = stepFour_discoverZero(resultThree)
        stepFive_discoverSix(resultZero)
        stepSix_discoverNine()
        val resultFour = stepSeven_discoverFour(resultZero)
        stepEight_discoverFive(resultFour)
        stepNine_discoverRemainingNumbers()
        return numbers
    }

    fun stepOne_discoverOne(): List<Deduction> {
        val one = remainingSignal.first { it.length == 2 }
        remainingSignal.remove(one)
        numbers[one.sortAlphabetically()] = 1
        val letters = one.toCharArray()
        return listOf(
            Deduction(mapOf(Segment.TOP_RIGHT to letters[0], Segment.BOTTOM_RIGHT to letters[1])),
            Deduction(mapOf(Segment.TOP_RIGHT to letters[1], Segment.BOTTOM_RIGHT to letters[0])),
        )
    }

    fun stepTwo_discoverSeven(input: List<Deduction>): List<Deduction> {
        val seven = remainingSignal.first { it.length == 3 }
        remainingSignal.remove(seven)
        numbers[seven.sortAlphabetically()] = 7
        val letters = seven.toCharArray().toList()
        val inputLetters = input.first().values.values.toList()
        val remainingLetter = (letters - inputLetters).first()
        return listOf(
            Deduction(
                mapOf(
                    Segment.TOP to remainingLetter,
                    Segment.TOP_RIGHT to inputLetters[0],
                    Segment.BOTTOM_RIGHT to inputLetters[1]
                )
            ),
            Deduction(
                mapOf(
                    Segment.TOP to remainingLetter,
                    Segment.TOP_RIGHT to inputLetters[1],
                    Segment.BOTTOM_RIGHT to inputLetters[0]
                )
            ),
        )
    }

    fun stepThree_discoverThree(input: List<Deduction>): List<Deduction> {
        val topRightLetter = input.first().values.getOrDefault(Segment.TOP_RIGHT, ' ')
        val bottomRightLetter = input.first().values.getOrDefault(Segment.BOTTOM_RIGHT, ' ')
        val three = remainingSignal
            .filter { it.length == 5 }
            .first { it.contains(topRightLetter) && it.contains(bottomRightLetter) }
        remainingSignal.remove(three)
        numbers[three.sortAlphabetically()] = 3
        val letters = three.toCharArray().toList()
        val inputLetters = input.first().values.values.toList()
        val remainingLetters = letters - inputLetters
        val deductions = mutableListOf<Deduction>()
        input.forEach {
            deductions.add(
                Deduction(
                    values = it.values + mapOf(
                        Segment.CENTER to remainingLetters[0],
                        Segment.BOTTOM to remainingLetters[1]
                    )
                )
            )
            deductions.add(
                Deduction(
                    values = it.values + mapOf(
                        Segment.CENTER to remainingLetters[1],
                        Segment.BOTTOM to remainingLetters[0]
                    )
                )
            )
        }
        return deductions
    }

    fun stepFour_discoverZero(input: List<Deduction>): List<Deduction> {
        val centerLetters = input.map { it.values[Segment.CENTER] }.distinct()
        val zero = remainingSignal
            .filter { it.length == 6 }
            .first { !(it.contains(centerLetters[0]!!) && it.contains(centerLetters[1]!!)) }
        remainingSignal.remove(zero)
        numbers[zero.sortAlphabetically()] = 0
        val bottomLetter = zero.toCharArray().first { centerLetters.contains(it) }
        val letters = zero.toCharArray().toList()
        val inputLetters = input.first().values.values.toList()
        val remainingLetters = letters - inputLetters
        val deductions = mutableListOf<Deduction>()
        input.forEach {
            if (it.values[Segment.BOTTOM] == bottomLetter) {
                deductions.add(
                    Deduction(
                        values = it.values + mapOf(
                            Segment.TOP_LEFT to remainingLetters[0],
                            Segment.BOTTOM_LEFT to remainingLetters[1]
                        )
                    )
                )
                deductions.add(
                    Deduction(
                        values = it.values + mapOf(
                            Segment.TOP_LEFT to remainingLetters[1],
                            Segment.BOTTOM_LEFT to remainingLetters[0]
                        )
                    )
                )
            }
        }
        return deductions
    }

    fun stepFive_discoverSix(input: List<Deduction>) {
        val topRightLetter = input.first().values.getOrDefault(Segment.TOP_RIGHT, ' ')
        val bottomRightLetter = input.first().values.getOrDefault(Segment.BOTTOM_RIGHT, ' ')
        val six = remainingSignal
            .filter { it.length == 6 }
            .first {
                !(it.contains(topRightLetter) && it.contains(bottomRightLetter)) && (
                    it.contains(topRightLetter) || it.contains(
                        bottomRightLetter
                    )
                    )
            }
        remainingSignal.remove(six)
        numbers[six.sortAlphabetically()] = 6
    }

    fun stepSix_discoverNine() {
        val nine = remainingSignal
            .first { it.length == 6 }
        remainingSignal.remove(nine)
        numbers[nine.sortAlphabetically()] = 9
    }

    fun stepSeven_discoverFour(input: List<Deduction>): List<Deduction> {
        val quatre = remainingSignal.first { it.length == 4 }
        remainingSignal.remove(quatre)
        numbers[quatre.sortAlphabetically()] = 4
        return input.filter { quatre.contains(it.values[Segment.TOP_LEFT]!!) }
    }

    fun stepEight_discoverFive(input: List<Deduction>) {
        val topLeftLetter = input.first().values.getOrDefault(Segment.TOP_LEFT, ' ')
        val bottomLeftLetter = input.first().values.getOrDefault(Segment.BOTTOM_LEFT, ' ')
        val five = remainingSignal
            .filter { it.length == 5 }
            .first {
                it.contains(topLeftLetter) && !it.contains(bottomLeftLetter)
            }
        remainingSignal.remove(five)
        numbers[five.sortAlphabetically()] = 5
    }

    fun stepNine_discoverRemainingNumbers() {
        val deux = remainingSignal.first { it.length == 5 }
        remainingSignal.remove(deux)
        numbers[deux.sortAlphabetically()] = 2
        val huit = remainingSignal.first()
        remainingSignal.remove(huit)
        numbers[huit.sortAlphabetically()] = 8
    }

    fun decode(): Int {
        this.analyzeSignal()
        return output
            .map { numbers[it] }
            .joinToString("")
            .toInt()
    }
}

fun String.sortAlphabetically(): String {
    val sortedSring = this.toCharArray()
    sortedSring.sort()
    return sortedSring.joinToString("")
}

data class Deduction(val values: Map<Segment, Char> = emptyMap())

enum class Segment {
    TOP,
    TOP_LEFT,
    TOP_RIGHT,
    CENTER,
    BOTTOM_LEFT,
    BOTTOM_RIGHT,
    BOTTOM
}
