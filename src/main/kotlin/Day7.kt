import java.io.InputStream
import kotlin.math.abs

fun main() {
    val day7 = Day7()
    existingTags.forEach { tag ->
        day7.run("day7-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 7 Part 1 : ${it.first.part1}")
            println("[$tag] Day 7 Part 2 : ${it.first.part2}")
        }
    }
}

class Day7 : Day<Int, Int, Int>() {
    override fun readData(data: InputStream) =
        data.bufferedReader().readLines()
            .first()
            .split(",")
            .map { it.toInt() }

    override fun runPart1(lines: List<Int>) = findBestPositionFor(lines).second

    override fun runPart2(lines: List<Int>) = findBestPositionForIncremental(lines).second
}

fun findBestPositionFor(crabs: List<Int>): Pair<Int, Int> {
    val minCrab = crabs.minOrNull() ?: 0
    val maxCrab = crabs.maxOrNull() ?: 0
    val deltas = mutableMapOf<Int, Int>()
    (minCrab..maxCrab).forEach { position ->
        crabs.forEach { crab -> deltas[position] = deltas.getOrDefault(position, 0) + abs(crab - position) }
    }
    val sortedDeltas = deltas.toList().sortedBy { (_, value) -> value }.toMap()
    val firstDeltaKey = sortedDeltas.keys.first()
    return Pair(firstDeltaKey, sortedDeltas[firstDeltaKey]!!)
}

fun findBestPositionForIncremental(crabs: List<Int>): Pair<Int, Int> {
    val minCrab = crabs.minOrNull() ?: 0
    val maxCrab = crabs.maxOrNull() ?: 0
    val deltas = mutableMapOf<Int, Int>()
    (minCrab..maxCrab).forEach { position ->
        crabs.forEach { crab ->
            deltas[position] = deltas.getOrDefault(position, 0) + calculateIncrementalDelta(crab, position)
        }
    }
    val sortedDeltas = deltas.toList().sortedBy { (_, value) -> value }.toMap()
    val firstDeltaKey = sortedDeltas.keys.first()
    return Pair(firstDeltaKey, sortedDeltas[firstDeltaKey]!!)
}

private fun calculateIncrementalDelta(start: Int, end: Int): Int {
    val diff = abs(start - end)
    val isEven = diff % 2 == 0
    return if (isEven) {
        diff * diff / 2 + diff / 2
    } else {
        diff * (diff - 1) / 2 + diff
    }
}
