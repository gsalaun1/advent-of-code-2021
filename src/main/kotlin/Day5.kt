import java.io.InputStream

fun main() {
    val day5 = Day5()
    existingTags.forEach { tag ->
        day5.run("day5-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 5 Part 1 : ${it.first.part1}")
            println("[$tag] Day 5 Part 2 : ${it.first.part2}")
        }
    }
}

class Day5 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val grid = VentGrid()
        grid.addVents(lines)
        return grid.overlappingCriteriaCount(2)
    }

    override fun runPart2(lines: List<String>): Int {
        val grid = VentGrid(true)
        grid.addVents(lines)
        return grid.overlappingCriteriaCount(2)
    }
}

class VentGrid(private val consideringDiagonals: Boolean = false) {

    private var grid = sortedSetOf<VentPoint>()

    private var overlappingGrid = mutableMapOf<VentPoint, Int>()

    fun addVents(vents: List<String>) {
        vents.forEach { this.addVent(it) }
    }

    fun addVent(vent: String) {
        val points = vent.split("->")
        val startPoint = VentPoint.buildFrom(points[0])
        val endPoint = VentPoint.buildFrom(points[1])
        generatePointsBetween(startPoint, endPoint)
    }

    private fun generatePointsBetween(startPoint: VentPoint, endPoint: VentPoint) {
        val minPoint = minOf(startPoint, endPoint)
        val maxPoint = maxOf(startPoint, endPoint)
        if (startPoint.y == endPoint.y) {
            val diffX = maxPoint.x - minPoint.x
            (minPoint.x..minPoint.x + diffX).forEach {
                val point = VentPoint(it, minPoint.y)
                grid.add(point)
                addOverlappingPoint(point)
            }
        } else if (startPoint.x == endPoint.x) {
            val diffY = maxPoint.y - minPoint.y
            (minPoint.y..minPoint.y + diffY).forEach {
                val point = VentPoint(minPoint.x, it)
                grid.add(point)
                addOverlappingPoint(point)
            }
        } else if (consideringDiagonals) {
            if (startPoint.x < endPoint.x && startPoint.y < endPoint.y) {
                val nbPointsToGenerate = maxPoint.x - minPoint.x
                (0..nbPointsToGenerate).forEach {
                    val point = VentPoint(startPoint.x + it, startPoint.y + it)
                    grid.add(point)
                    addOverlappingPoint(point)
                }
            }
            if (startPoint.x < endPoint.x && startPoint.y > endPoint.y) {
                val nbPointsToGenerate = maxPoint.x - minPoint.x
                (0..nbPointsToGenerate).forEach {
                    val point = VentPoint(startPoint.x + it, startPoint.y - it)
                    grid.add(point)
                    addOverlappingPoint(point)
                }
            }
            if (startPoint.x > endPoint.x && startPoint.y > endPoint.y) {
                val nbPointsToGenerate = maxPoint.x - minPoint.x
                (0..nbPointsToGenerate).forEach {
                    val point = VentPoint(startPoint.x - it, startPoint.y - it)
                    grid.add(point)
                    addOverlappingPoint(point)
                }
            }
            if (startPoint.x > endPoint.x && startPoint.y < endPoint.y) {
                val nbPointsToGenerate = maxPoint.x - minPoint.x
                (0..nbPointsToGenerate).forEach {
                    val point = VentPoint(startPoint.x - it, startPoint.y + it)
                    grid.add(point)
                    addOverlappingPoint(point)
                }
            }
        }
    }

    private fun addOverlappingPoint(point: VentPoint) {
        if (overlappingGrid[point] == null) {
            overlappingGrid[point] = 0
        }
        overlappingGrid[point] = overlappingGrid[point]!! + 1
    }

    fun overlappingReport(): String {
        val maxX = grid.maxOf { it.x }
        val maxY = grid.maxOf { it.y }
        val lines = mutableListOf<List<String>>()
        (0..maxY).forEach { currentY ->
            lines.add(
                (0..maxX).map { currentX ->
                    if (overlappingGrid[
                        VentPoint(
                                currentX,
                                currentY
                            )
                    ] != null
                    ) "${overlappingGrid[VentPoint(currentX, currentY)]}" else "."
                }
            )
        }
        return lines.joinToString("\n") { it.joinToString("") }
    }

    fun overlappingCriteriaCount(criteria: Int) = overlappingGrid.filter { entry -> entry.value >= criteria }.count()

    override fun toString(): String {
        if (grid.isEmpty()) {
            return ""
        }
        val maxX = grid.maxOf { it.x }
        val maxY = grid.maxOf { it.y }
        val lines = mutableListOf<List<String>>()
        (0..maxY).forEach { currentY ->
            lines.add(
                (0..maxX).map { currentX -> if (grid.any { it.x == currentX && it.y == currentY }) "v" else "." }
            )
        }
        return lines.joinToString("\n") { it.joinToString("") }
    }

    private data class VentPoint(val x: Int, val y: Int) : Comparable<VentPoint> {
        companion object {
            fun buildFrom(point: String): VentPoint {
                val coordinates = point.trim().split(",").map { it.toInt() }
                return VentPoint(coordinates[0], coordinates[1])
            }
        }

        override fun compareTo(other: VentPoint): Int {
            if (x == other.x) {
                return y - other.y
            }
            return x - other.x
        }
    }
}
