import java.io.InputStream

fun main() {
    val day2 = Day2()
    existingTags.forEach { tag ->
        day2.run("day2-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 2 Part 1 : ${it.first.part1}")
            println("[$tag] Day 2 Part 2 : ${it.first.part2}")
        }
    }
}

class Day2 : Day<String, Long, Long>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Long {
        val submarine = Submarine()
        lines.forEach { submarine.movementWithoutAim(it) }
        return (submarine.horizontalPosition * submarine.depth).toLong()
    }

    override fun runPart2(lines: List<String>): Long {
        val submarine = Submarine()
        lines.forEach { submarine.movementWithAim(it) }
        return (submarine.horizontalPosition * submarine.depth).toLong()
    }
}

class Submarine {
    var horizontalPosition = 0
    var depth = 0
    private var aim = 0

    fun movementWithoutAim(instruction: String) {
        val directionAndValue = instruction.split(" ")
        when (directionAndValue[0]) {
            "forward" -> horizontalPosition += directionAndValue[1].toInt()
            "down" -> depth += directionAndValue[1].toInt()
            "up" -> depth -= directionAndValue[1].toInt()
        }
    }

    fun movementWithAim(instruction: String) {
        val directionAndValue = instruction.split(" ")
        when (directionAndValue[0]) {
            "forward" -> {
                horizontalPosition += directionAndValue[1].toInt()
                depth += aim * directionAndValue[1].toInt()
            }
            "down" -> aim += directionAndValue[1].toInt()
            "up" -> aim -= directionAndValue[1].toInt()
        }
    }
}
