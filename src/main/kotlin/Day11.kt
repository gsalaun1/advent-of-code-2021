import java.io.InputStream

fun main() {
    val day11 = Day11()
    existingTags.forEach { tag ->
        day11.run("day11-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 11 Part 1 : ${it.first.part1}")
            println("[$tag] Day 11 Part 2 : ${it.first.part2}")
        }
    }
}

class Day11 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val octopuses = Octopuses(lines)
        return octopuses.countFlashingOctopusesAfterSteps(100)
    }

    override fun runPart2(lines: List<String>): Int {
        val octopuses = Octopuses(lines)
        return octopuses.findFirstSynchronizingStep()
    }
}

class Octopuses {

    private var octopuses: List<Octopus>

    constructor(lines: List<String>) {
        octopuses = lines.flatMapIndexed { row, line ->
            line.toCharArray().mapIndexed { column, value ->
                Octopus(column, row, value.toString().toInt())
            }
        }
    }

    constructor(lines: String) {
        octopuses = lines.split("\n").flatMapIndexed { row, line ->
            line.toCharArray().mapIndexed { column, value ->
                Octopus(column, row, value.toString().toInt())
            }
        }
    }

    constructor(inOctopuses: Collection<Octopus>) {
        octopuses = inOctopuses.toList()
    }

    private fun increaseNeighborsOfFlashingOctopuses(flashingOctopusesToIgnore: Set<Octopus>): Octopuses {
        return Octopuses(
            this.octopuses.map { octopus ->
                if (octopus.isFlashing()) {
                    octopus
                } else {
                    val flashingNeighbors = this.neighborsOf(octopus)
                        .filter { it.isFlashing() }
                        .filter { !flashingOctopusesToIgnore.contains(it) }
                    (0 until flashingNeighbors.size).fold(octopus) { flashingNeighbor, _ ->
                        if (!flashingNeighbor.isFlashing()) {
                            flashingNeighbor.increaseEnergy()
                        } else {
                            flashingNeighbor
                        }
                    }
                }
            }
        )
    }

    fun generateNextStep(): Octopuses {
        var nextStepOctopuses = Octopuses(octopuses.map { it.increaseEnergy() })
        var newFlashingOctopuses = nextStepOctopuses.octopuses.filter { it.isFlashing() }.toSet()
        val flashingOctopusesToIgnore = mutableSetOf<Octopus>()
        while (newFlashingOctopuses.isNotEmpty()) {
            nextStepOctopuses = nextStepOctopuses.increaseNeighborsOfFlashingOctopuses(flashingOctopusesToIgnore)
            flashingOctopusesToIgnore += newFlashingOctopuses
            newFlashingOctopuses = nextStepOctopuses.octopuses
                .filter { it.isFlashing() }
                .filter { !flashingOctopusesToIgnore.contains(it) }
                .toSet()
        }
        return nextStepOctopuses
    }

    fun generateNextSteps(steps: Int): Octopuses {
        return (0 until steps).fold(this) { acc, _ ->
            acc.generateNextStep()
        }
    }

    fun countFlashingOctopusesAfterSteps(steps: Int): Int {
        var flashingOctopuses = this.octopuses.count { it.isFlashing() }
        (0 until steps).fold(this) { acc, _ ->
            val nextStep = acc.generateNextStep()
            flashingOctopuses += nextStep.octopuses.count { it.isFlashing() }
            nextStep
        }
        return flashingOctopuses
    }

    private fun allOctopusesFlashing() =
        this.octopuses.filter { it.isFlashing() }.size == this.octopuses.size

    fun findFirstSynchronizingStep(): Int {
        var examinedOctopuses = this
        var allOctopusesSynchronized = examinedOctopuses.allOctopusesFlashing()
        var cpt = 0
        while (!allOctopusesSynchronized) {
            examinedOctopuses = examinedOctopuses.generateNextStep()
            allOctopusesSynchronized = examinedOctopuses.allOctopusesFlashing()
            cpt++
        }
        return cpt
    }

    fun neighborsOf(octopus: Octopus) =
        octopuses.filter { it.x == octopus.x - 1 && it.y == octopus.y - 1 }.toSet() +
            octopuses.filter { it.x == octopus.x && it.y == octopus.y - 1 }.toSet() +
            octopuses.filter { it.x == octopus.x + 1 && it.y == octopus.y - 1 }.toSet() +
            octopuses.filter { it.x == octopus.x - 1 && it.y == octopus.y }.toSet() +
            octopuses.filter { it.x == octopus.x + 1 && it.y == octopus.y }.toSet() +
            octopuses.filter { it.x == octopus.x - 1 && it.y == octopus.y + 1 }.toSet() +
            octopuses.filter { it.x == octopus.x && it.y == octopus.y + 1 }.toSet() +
            octopuses.filter { it.x == octopus.x + 1 && it.y == octopus.y + 1 }.toSet()

    override fun toString(): String {
        return octopuses
            .map { it.y }
            .distinct()
            .joinToString("\n") { row ->
                octopuses
                    .filter { it.y == row }
                    .map { it.energy }
                    .joinToString("")
            }
    }

    data class Octopus(val x: Int, val y: Int, val energy: Int) {
        fun increaseEnergy() = Octopus(x, y, if (energy == 9) 0 else energy + 1)

        fun isFlashing() = energy == 0
    }
}
