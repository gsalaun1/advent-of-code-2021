import java.io.InputStream

fun main() {
    val day6 = Day6()
    existingTags.forEach { tag ->
        day6.run("day6-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 6 Part 1 : ${it.first.part1}")
            println("[$tag] Day 6 Part 2 : ${it.first.part2}")
        }
    }
}

class Day6 : Day<Int, Long, Long>() {
    override fun readData(data: InputStream) =
        data.bufferedReader().readLines()
            .first()
            .split(",")
            .map { it.toInt() }

    override fun runPart1(lines: List<Int>) = countFishesAfterDays(80, lines)

    override fun runPart2(lines: List<Int>) = countFishesAfterDays(256, lines)
}

fun passADayFor(population: Map<Int, Long>): Map<Int, Long> {
    val generatedPopulation = mutableMapOf<Int, Long>()
    (0..8).forEach {
        generatedPopulation[it - 1] = population.getOrDefault(it, 0)
    }
    val nbFishesCreated = generatedPopulation.getOrDefault(-1, 0)
    generatedPopulation[8] = nbFishesCreated
    generatedPopulation[6] = generatedPopulation.getOrDefault(6, 0) + nbFishesCreated
    generatedPopulation.remove(-1)
    return generatedPopulation.toMap()
}

fun passDaysFor(days: Int, population: List<Int>): Map<Int, Long> {
    val initialPopulation = mutableMapOf<Int, Long>(
        0 to 0,
        1 to 0,
        2 to 0,
        3 to 0,
        4 to 0,
        5 to 0,
        6 to 0,
        7 to 0,
        8 to 0
    )
    population.forEach {
        initialPopulation[it] = initialPopulation.getOrDefault(it, 0) + 1
    }
    return (0 until days).fold(
        initialPopulation.toMap()
    ) { acc, _ ->
        passADayFor(acc)
    }
}

fun countFishesAfterDays(days: Int, population: List<Int>) =
    passDaysFor(days, population).map { it.value }.sum()
