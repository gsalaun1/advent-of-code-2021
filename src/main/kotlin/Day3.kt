import java.io.InputStream
import kotlin.math.pow

fun main() {
    val day3 = Day3()
    existingTags.forEach { tag ->
        day3.run("day3-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 3 Part 1 : ${it.first.part1}")
            println("[$tag] Day 3 Part 2 : ${it.first.part2}")
        }
    }
}

class Day3 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val lineLength = lines.first().length
        val bits = (0 until lineLength).fold(emptyList<Pair<Int, Int>>()) { acc, value ->
            acc + computeColumn(lines.map { it[value].toString().toInt() })
        }
        val minMax = bitsToMinMax(bits)
        val gammaRate = minMax.first.toBaseTen()
        val epsilonRate = minMax.second.toBaseTen()
        return gammaRate * epsilonRate
    }

    override fun runPart2(lines: List<String>): Int {
        val oxygen = lines.filterMostPresent()[0].toBaseTen()
        val co2 = lines.filterLeastPresent()[0].toBaseTen()
        return oxygen * co2
    }
}

fun computeColumn(column: List<Int>) =
    column.fold(Pair(0, 0)) { acc, value ->
        if (value == 0) {
            Pair(acc.first + 1, acc.second)
        } else {
            Pair(acc.first, acc.second + 1)
        }
    }

fun bitsToMinMax(bits: List<Pair<Int, Int>>) =
    bits.fold(Pair("", "")) { acc, value ->
        if (value.first < value.second) {
            Pair(acc.first + "0", acc.second + "1")
        } else {
            Pair(acc.first + "1", acc.second + "0")
        }
    }

fun String.toBaseTen() =
    this.reversed().foldIndexed(0) { index, acc, value ->
        acc + value.toString().toInt() * 2.0.pow(index.toDouble()).toInt()
    }

fun List<String>.filterMostPresent(startIndex: Int = 0): List<String> {
    val filteredList = filterMostPresentAtIndex(startIndex)
    if (filteredList.size > 1) {
        return filteredList.filterMostPresent(startIndex + 1)
    }
    return filteredList
}

fun List<String>.filterMostPresentAtIndex(index: Int): List<String> {
    val sortedColumns: Map<String, List<String>> = this.sortColumnByBitForIndex(index)
    val column0 = sortedColumns["0"] ?: emptyList()
    val column1 = sortedColumns["1"] ?: emptyList()
    return if (column1.size >= column0.size) {
        column1
    } else {
        column0
    }
}

fun List<String>.sortColumnByBitForIndex(index: Int): Map<String, List<String>> {
    return this.fold(mapOf("0" to mutableListOf<String>(), "1" to mutableListOf<String>())) { acc, value ->
        if (value[index] == '0') {
            acc["0"]?.add(value)
        } else {
            acc["1"]?.add(value)
        }
        acc
    }
}

fun List<String>.filterLeastPresent(startIndex: Int = 0): List<String> {
    if (this.size == 1) {
        return this
    }
    val filteredList = filterLeastPresentAtIndex(startIndex)
    if (filteredList.size > 1) {
        return filteredList.filterLeastPresent(startIndex + 1)
    }
    return filteredList
}

fun List<String>.filterLeastPresentAtIndex(index: Int): List<String> {
    val sortedColumns: Map<String, List<String>> = this.sortColumnByBitForIndex(index)
    val column0 = sortedColumns["0"] ?: emptyList()
    val column1 = sortedColumns["1"] ?: emptyList()
    return if (column1.size < column0.size) {
        column1
    } else {
        column0
    }
}
