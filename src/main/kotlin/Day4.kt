import java.io.InputStream

fun main() {
    val day4 = Day4()
    existingTags.forEach { tag ->
        day4.run("day4-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 4 Part 1 : ${it.first.part1}")
            println("[$tag] Day 4 Part 2 : ${it.first.part2}")
        }
    }
}

class Day4 : Day<String, Int, Int?>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val tokens = buildTokens(lines)
        val grids = buildGrids(lines)
        var winningGrid: BingoGrid? = null
        var cptNumbers = 0
        var token = tokens[cptNumbers]
        while (winningGrid == null) {
            grids.forEach { it.pushToken(token) }
            if (grids.any { it.isAWinner() }) {
                winningGrid = grids.first { it.isAWinner() }
            } else {
                cptNumbers++
                token = tokens[cptNumbers]
            }
        }
        return winningGrid.sumUncheckedCells() * token
    }

    override fun runPart2(lines: List<String>): Int? {
        val tokens = buildTokens(lines)
        val grids = buildGrids(lines).toMutableList()
        val winningGrids = mutableMapOf<Int, BingoGrid>()
        tokens.forEach { token ->
            grids.forEach { it.pushToken(token) }
            grids.filter { it.isAWinner() }.forEach {
                winningGrids[token] = it
                grids.remove(it)
            }
        }
        val lastToken = winningGrids.keys.last()
        val lastGridSum = winningGrids.getOrDefault(lastToken, BingoGrid()).sumUncheckedCells()
        return lastGridSum * lastToken
    }
}

private fun buildTokens(lines: List<String>) = lines[0].split(",").map { it.toInt() }

private fun buildGrids(lines: List<String>) =
    lines.foldIndexed(emptyList<BingoGrid>()) { index, acc, line ->
        if (index == 0 || line == "") {
            acc
        } else {
            if (acc.isEmpty()) {
                val newBingoGrid = BingoGrid()
                newBingoGrid.addLine(line)
                acc + newBingoGrid
            } else {
                val bingoGrid = acc[acc.size - 1]
                if (bingoGrid.isComplete()) {
                    val newBingoGrid = BingoGrid()
                    newBingoGrid.addLine(line)
                    acc + newBingoGrid
                } else {
                    bingoGrid.addLine(line)
                    acc
                }
            }
        }
    }

class BingoGrid {

    private var lines = mutableListOf<List<Cell>>()

    fun addLine(line: String) {
        lines.add(line.trim().replace("  ", " ").split(" ").map { Cell(it.toInt()) })
    }

    fun lines(): List<List<Int>> {
        return lines.map { line -> line.map { it.value } }
    }

    fun isComplete(): Boolean {
        if (lines.isEmpty()) {
            return false
        }
        val nbColumns = lines[0].size
        return nbColumns == lines.size
    }

    fun pushToken(token: Int) {
        lines.forEach { line -> line.filter { cell -> cell.value == token }.forEach { it.check() } }
    }

    override fun toString() =
        lines.fold("") { acc, line ->
            acc + line + "\n"
        }

    fun isAWinner(): Boolean {
        val horizontalWinner = lines.any { it.isAWinner() }
        return if (horizontalWinner) {
            true
        } else {
            val countColumns = lines[0].size
            val verticalLines = (0 until countColumns).fold(mutableListOf<List<Cell>>()) { acc, value ->
                acc.add(lines.map { it[value] })
                acc
            }
            verticalLines.any { it.isAWinner() }
        }
    }

    private fun List<Cell>.isAWinner() = this.all { it.isChecked() }

    fun sumUncheckedCells() =
        lines.fold(0) { acc, line ->
            acc + line.filter { !it.isChecked() }.sumOf { it.value }
        }

    private class Cell(val value: Int) {

        private var checked: Boolean = false

        fun check() {
            this.checked = true
        }

        fun isChecked() = checked

        override fun toString() = if (checked) "($value)" else "$value"
    }
}
