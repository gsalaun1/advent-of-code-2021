import java.io.InputStream

fun main() {
    val day13 = Day13()
    existingTags.forEach { tag ->
        day13.run("day13-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 13 Part 1 : ${it.first.part1}")
            println("[$tag] Day 13 Part 2 : ${it.first.part2}")
        }
    }
}

class Day13 : Day<String, Int, String>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val origamiFolder = OrigamiFolder(lines)
        return origamiFolder.applyFirstFoldInstruction().countVisiblePoints()
    }

    override fun runPart2(lines: List<String>): String {
        val origamiFolder = OrigamiFolder(lines)
        return origamiFolder.foldAndDecrypt()
    }
}

class OrigamiFolder(lines: List<String>) {

    private var origami: Origami

    private var foldInstructions: List<FoldInstruction>

    private var dictionary: OrigamiDictionary

    companion object {
        const val LETTER_WIDTH = 4
    }

    init {
        val gridInstructions = lines
            .filter { it.isNotEmpty() }
            .filter { !it.startsWith("fold") }
        origami = Origami(gridInstructions)
        foldInstructions = buildFoldInstructions(lines)
        dictionary = OrigamiDictionary(buildDictionary())
    }

    private fun buildFoldInstructions(lines: List<String>) =
        lines
            .filter { it.isNotEmpty() }
            .filter { it.startsWith("fold along") }
            .map { it.replace("fold along ", "") }
            .mapNotNull { foldInstruction ->
                val instructions = foldInstruction.split("=")
                when (instructions[0]) {
                    "x" -> FoldInstruction(FoldDirection.LEFT, instructions[1].toInt())
                    "y" -> FoldInstruction(FoldDirection.UP, instructions[1].toInt())
                    else -> null
                }
            }

    private fun buildDictionary(): Map<String, String> {
        val dictionaryLines =
            object {}.javaClass.getResourceAsStream("day13-dictionary.txt").bufferedReader().readLines()
        return dictionaryLines.chunked(8).associate { letterLines ->
            val letter = letterLines.first()
            val pattern = letterLines.subList(1, 7).joinToString("\n")
            Pair(pattern, letter)
        }
    }

    fun fold() = this.foldInstructions.fold(this.origami) { acc, instruction -> acc.fold(instruction) }

    fun foldAndDecrypt(): String {
        val foldedOrigami = this.fold()
        return dictionary.decrypt(foldedOrigami.toString())
    }

    fun applyFirstFoldInstruction() = this.origami.fold(this.foldInstructions.first())

    fun knownLetters(): List<String> {
        return dictionary.knownLetters()
    }

    data class FoldInstruction(val direction: FoldDirection, val axisIndex: Int)

    enum class FoldDirection {
        LEFT, UP
    }

    class Origami {

        private var points: Set<Point>
        private var lastRowIndex: Int = 0

        constructor(lines: List<String>) {
            this.points = lines.fold(emptySet()) { acc, value ->
                val coordonnees = value.split(",").map { it.toInt() }
                acc + Point(coordonnees[0], coordonnees[1])
            }
            this.lastRowIndex = points.maxOf { it.y }
        }

        constructor(points: Set<Point>, lastRowIndex: Int? = null) {
            this.points = points
            this.lastRowIndex = lastRowIndex ?: this.points.maxOf { it.y }
        }

        override fun toString(): String {
            val maxX = points.maxOf { it.x }
            return (0..lastRowIndex).joinToString("\n") { row ->
                (0..maxX).joinToString("") { column ->
                    if (points.contains(Point(column, row))) {
                        "#"
                    } else {
                        "."
                    }
                }
            }
        }

        fun fold(foldInstruction: FoldInstruction): Origami {
            val transformedPoints = when (foldInstruction.direction) {
                FoldDirection.UP -> {
                    points.fold(emptySet()) { acc, point ->
                        when {
                            point.y < foldInstruction.axisIndex -> {
                                acc + point
                            }
                            point.y == foldInstruction.axisIndex -> {
                                acc
                            }
                            else -> {
                                val newY = foldInstruction.axisIndex - (point.y - foldInstruction.axisIndex)
                                acc + Point(point.x, newY)
                            }
                        }
                    }
                }
                FoldDirection.LEFT -> {
                    points.fold(emptySet<Point>()) { acc, point ->
                        when {
                            point.x < foldInstruction.axisIndex -> {
                                acc + point
                            }
                            point.x == foldInstruction.axisIndex -> {
                                acc
                            }
                            else -> {
                                val newX = foldInstruction.axisIndex - (point.x - foldInstruction.axisIndex)
                                acc + Point(newX, point.y)
                            }
                        }
                    }
                }
            }
            val transformedLastRowIndex = when (foldInstruction.direction) {
                FoldDirection.UP -> foldInstruction.axisIndex - 1
                FoldDirection.LEFT -> lastRowIndex
            }
            return Origami(transformedPoints, transformedLastRowIndex)
        }

        fun countVisiblePoints() = points.size

        data class Point(val x: Int, val y: Int) : Comparable<Point> {
            override fun compareTo(other: Point) = compareBy<Point> { it.y }.thenBy { it.x }.compare(this, other)
        }
    }

    data class OrigamiDictionary(val letters: Map<String, String>) {
        fun decrypt(origamiRepresentation: String): String {
            val linesRepresentation = origamiRepresentation.split("\n")
            val nbLetters = (linesRepresentation.first().length + 1) / (LETTER_WIDTH + 1)
            return (0 until nbLetters)
                .map { letterIndex ->
                    val letterStart = letterIndex * LETTER_WIDTH + letterIndex
                    val letterEnd = letterStart + LETTER_WIDTH - 1
                    linesRepresentation.joinToString("\n") { it.substring(letterStart, letterEnd + 1) }
                }
                .mapNotNull {
                    letters[it]
                }
                .joinToString("")
        }

        fun knownLetters() = letters.values.toList()
    }
}
