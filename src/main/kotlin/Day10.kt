import java.io.InputStream

fun main() {
    val day10 = Day10()
    existingTags.forEach { tag ->
        day10.run("day10-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 10 Part 1 : ${it.first.part1}")
            println("[$tag] Day 10 Part 2 : ${it.first.part2}")
        }
    }
}

class Day10 : Day<String, Int, Long>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val chunkAnalyzer = ChunkAnalyzer(lines)
        return chunkAnalyzer.sumOfSyntaxErrorScoreBasedOnFirstIllegalCharacter()
    }

    override fun runPart2(lines: List<String>): Long {
        val chunkAnalyzer = ChunkAnalyzer(lines)
        return chunkAnalyzer.middleScoreForIncompleteLines()
    }
}

class ChunkAnalyzer(val lines: List<String>) {

    companion object {
        val CHUNK_PATTERNS = listOf("()", "[]", "{}", "<>")
        val OPENING_PATTERNS = listOf('(', '[', '{', '<')
        val CORRUPTED_PATTERNS_SCORE = mapOf(')' to 3, ']' to 57, '}' to 1197, '>' to 25137)
        val MATCHING_PATTERNS = mapOf('(' to ')', '[' to ']', '{' to '}', '<' to '>')
        val INCOMPLETE_PATTERNS_SCORE = mapOf(')' to 1, ']' to 2, '}' to 3, '>' to 4)
    }

    fun findFirstIncorrectClosingCharacterIn(line: String): Char? {
        var remainingLine = line
        while (CHUNK_PATTERNS.any { remainingLine.contains(it) }) {
            remainingLine = CHUNK_PATTERNS.fold(
                remainingLine
            ) { acc, value -> acc.replace(value, "") }
        }
        remainingLine = OPENING_PATTERNS.fold(remainingLine) { acc, value ->
            acc.replace(value, ' ')
        }.trim()
        return remainingLine.firstOrNull()
    }

    fun sumOfSyntaxErrorScoreBasedOnFirstIllegalCharacter(): Int {
        val countClosing = mutableMapOf<Char, Int>()
        lines.forEach {
            val incorrectCharacter = findFirstIncorrectClosingCharacterIn(it)
            if (incorrectCharacter != null) {
                countClosing[incorrectCharacter] = countClosing.getOrDefault(incorrectCharacter, 0) + 1
            }
        }
        var result = 0
        countClosing.forEach {
            result += it.value * CORRUPTED_PATTERNS_SCORE.getOrDefault(it.key, 0)
        }
        return result
    }

    fun findIncompleteLines(): List<String> {
        return lines.filter { findFirstIncorrectClosingCharacterIn(it) == null }
    }

    fun missingPieceIn(line: String): String {
        var remainingLine = line
        while (CHUNK_PATTERNS.any { remainingLine.contains(it) }) {
            remainingLine = CHUNK_PATTERNS.fold(
                remainingLine
            ) { acc, value -> acc.replace(value, "") }
        }
        return remainingLine.reversed().map { MATCHING_PATTERNS[it] }.joinToString("")
    }

    fun completionScoreFor(line: String): Long {
        val missingPiece = missingPieceIn(line)
        return missingPiece.fold(0) { acc, value ->
            acc * 5 + INCOMPLETE_PATTERNS_SCORE.getOrDefault(value, 0)
        }
    }

    fun middleScoreForIncompleteLines(): Long {
        val scores = findIncompleteLines().map { completionScoreFor(it) }.sorted()
        return scores[scores.size / 2]
    }
}
