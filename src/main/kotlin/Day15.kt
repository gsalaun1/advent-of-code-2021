import java.io.InputStream
import java.util.Comparator
import java.util.TreeSet
import kotlin.math.min

fun main() {
    val day15 = Day15()
    existingTags.forEach { tag ->
        day15.run("day15-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 15 Part 1 : ${it.first.part1}")
            println("[$tag] Day 15 Part 2 : ${it.first.part2}")
        }
    }
}

class Day15 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val chitonAvoider = CavernChitonAvoider(lines)
        return chitonAvoider.findBestPath()
    }

    override fun runPart2(lines: List<String>): Int {
        val chitonAvoider = CaveChitonAvoider(lines)
        return chitonAvoider.findBestPath()
    }
}

abstract class ChitonAvoider(lines: List<String>) {

    val points: Array<Array<WeightedRiskPoint>>

    private val pointsComparator: Comparator<Pair<Int, Int>>

    private val pointsToCompute: TreeSet<Pair<Int, Int>>

    init {
        points = buildWeightedRiskGrid(lines)
        pointsComparator = compareBy(
            { points[it.first][it.second].weight },
            { it.second },
            { it.first }
        )
        pointsToCompute = sortedSetOf(pointsComparator)
    }

    abstract fun buildWeightedRiskGrid(lines: List<String>): Array<Array<WeightedRiskPoint>>

    fun findBestPath(): Int {
        val maxX = points.size - 1
        val maxY = points.size - 1
        var searchedPoint = points[maxX][maxY]
        while (!searchedPoint.computed) {
            dijkstraStep()
            searchedPoint = points[maxX][maxY]
        }
        val startWeight = points[0][0].weight
        val endWeight = points[maxX][maxY].weight
        return endWeight - startWeight
    }

    fun dijkstraStep() {
        val referenceCoordinates = if (pointsToCompute.isEmpty()) {
            Pair(0, 0)
        } else {
            pointsToCompute.first()
        }

        val referencePoint = points[referenceCoordinates.first][referenceCoordinates.second]
        val neighborsToCompute =
            notComputedNeighbors(referenceCoordinates.first, referenceCoordinates.second)

        neighborsToCompute.forEach { point ->
            val currentPoint = points[point.first][point.second].copy()
            points[point.first][point.second] =
                currentPoint.copy(weight = min(currentPoint.weight, referencePoint.weight + currentPoint.risk))
        }
        points[referenceCoordinates.first][referenceCoordinates.second] = referencePoint.copy(computed = true)

        pointsToCompute.addAll(notComputedNeighbors(referenceCoordinates.first, referenceCoordinates.second))
        pointsToCompute.remove(referenceCoordinates)
    }

    fun notComputedNeighbors(
        x: Int,
        y: Int
    ): List<Pair<Int, Int>> {
        val leftPoint = if (x > 0) Pair(x - 1, y) else null
        val rightPoint = if (x < points.size - 1) Pair(x + 1, y) else null
        val topPoint = if (y > 0) Pair(x, y - 1) else null
        val bottomPoint = if (y < points.size - 1) Pair(x, y + 1) else null
        return listOfNotNull(
            leftPoint,
            rightPoint,
            topPoint,
            bottomPoint
        ).filter { !points[it.first][it.second].computed }
    }

    override fun toString(): String {
        val maxX = points.size - 1
        val maxY = points.size - 1
        return (0..maxY).joinToString("\n") { y ->
            (0..maxX).joinToString("") { x ->
                points[x][y].risk.toString()
            }
        }
    }
}

data class WeightedRiskPoint(
    val risk: Int,
    val weight: Int = Int.MAX_VALUE,
    val computed: Boolean = false
) {
    fun increaseRisk(increaseStep: Int) =
        this.copy(risk = if (risk + increaseStep > 9) risk + increaseStep - 9 else risk + increaseStep)
}

class CavernChitonAvoider(lines: List<String>) : ChitonAvoider(lines) {
    override fun buildWeightedRiskGrid(lines: List<String>): Array<Array<WeightedRiskPoint>> {
        val generatedPoints = Array(lines.size) { arrayOfNulls<WeightedRiskPoint>(lines.size) }
        lines.forEachIndexed { row, line ->
            line.toCharArray().forEachIndexed { column, value ->
                if (column == 0 && row == 0) {
                    generatedPoints[column][row] =
                        WeightedRiskPoint(value.toString().toInt(), value.toString().toInt())
                } else {
                    generatedPoints[column][row] = WeightedRiskPoint(value.toString().toInt())
                }
            }
        }
        return generatedPoints as Array<Array<WeightedRiskPoint>>
    }
}

class CaveChitonAvoider(lines: List<String>) : ChitonAvoider(lines) {
    override fun buildWeightedRiskGrid(lines: List<String>): Array<Array<WeightedRiskPoint>> {
        val initialGrid = Array(lines.size) { arrayOfNulls<WeightedRiskPoint>(lines.size) }
        lines.forEachIndexed { row, line ->
            line.toCharArray().forEachIndexed { column, value ->
                initialGrid[column][row] = WeightedRiskPoint(value.toString().toInt())
            }
        }

        val horizontalReplica = replicateAndIncrementHorizontally(initialGrid as Array<Array<WeightedRiskPoint>>)

        val verticalReplica = replicateAndIncrementVertically(horizontalReplica)

        verticalReplica[0][0] = verticalReplica[0][0].copy(weight = verticalReplica[0][0].risk)

        return verticalReplica
    }

    private fun replicateAndIncrementHorizontally(referenceGrid: Array<Array<WeightedRiskPoint>>): Array<Array<WeightedRiskPoint>> {
        val generatedGrid = Array(referenceGrid.size * 5) { arrayOfNulls<WeightedRiskPoint>(referenceGrid.size) }
        val maxX = referenceGrid[0].size - 1
        val maxY = referenceGrid.size - 1
        (0 until 5).forEach { gridIndex ->
            (0..maxY).forEach { row ->
                (0..maxX).forEach { column ->
                    generatedGrid[column + referenceGrid.size * gridIndex][row] =
                        referenceGrid[column][row].increaseRisk(gridIndex)
                }
            }
        }
        return generatedGrid as Array<Array<WeightedRiskPoint>>
    }

    private fun replicateAndIncrementVertically(referenceGrid: Array<Array<WeightedRiskPoint>>): Array<Array<WeightedRiskPoint>> {
        val generatedGrid = Array(referenceGrid.size) { arrayOfNulls<WeightedRiskPoint>(referenceGrid.size * 5) }
        val maxX = referenceGrid.size - 1
        val maxY = referenceGrid[0].size - 1
        (0 until 5).forEach { gridIndex ->
            (0..maxY).forEach { row ->
                (0..maxX).forEach { column ->
                    generatedGrid[column][row + referenceGrid[0].size * gridIndex] =
                        referenceGrid[column][row].increaseRisk(gridIndex)
                }
            }
        }
        return generatedGrid as Array<Array<WeightedRiskPoint>>
    }
}
