import java.io.InputStream

fun main() {
    val day1 = Day1()
    existingTags.forEach { tag ->
        day1.run("day1-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 1 Part 1 : ${it.first.part1}")
            println("[$tag] Day 1 Part 2 : ${it.first.part2}")
        }
    }
}

class Day1 : Day<Int, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines().map { it.toInt() }

    override fun runPart1(lines: List<Int>) = lines.nbLinesWithIncrease()

    override fun runPart2(lines: List<Int>): Int {
        val windowSums = lines.mapIndexedNotNull { index, _ ->
            if (index < lines.size - 2) {
                lines[index] + lines[index + 1] + lines[index + 2]
            } else {
                null
            }
        }
        return windowSums.nbLinesWithIncrease()
    }

    private fun List<Int>.nbLinesWithIncrease() =
        this.filterIndexed { index, element -> index > 0 && element > this[index - 1] }.count()
}
