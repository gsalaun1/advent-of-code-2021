import java.io.InputStream
import java.util.Locale

fun main() {
    val day12 = Day12()
    existingTags.forEach { tag ->
        day12.run("day12-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 12 Part 1 : ${it.first.part1}")
            println("[$tag] Day 12 Part 2 : ${it.first.part2}")
        }
    }
}

class Day12 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val pathFinder = PathFinder(lines)
        return pathFinder.findDistinctPaths().size
    }

    override fun runPart2(lines: List<String>): Int {
        val pathFinder = PathFinder(lines)
        return pathFinder.findDistinctPathsAllowingTwoVisitsInASmallCave().size
    }
}

class PathFinder(val lines: List<String>) {

    private val nodes: MutableMap<String, Set<String>> = mutableMapOf()

    init {
        lines.forEach {
            val extremities = it.split("-")
            val start = extremities[0]
            val end = extremities[1]
            nodes[start] = nodes.getOrDefault(start, emptySet()) + end
            if (end != END && start != START) {
                nodes[end] = nodes.getOrDefault(end, emptySet()) + start
            }
        }
    }

    companion object {
        const val START = "start"
        const val END = "end"
    }

    fun findDistinctPaths() = findPathsStartingBy(START).map { it.joinToString(",") }

    private fun findPathsStartingBy(value: String, nodesToExclude: Set<String> = emptySet()): List<List<String>> {
        if (value == END) {
            return listOf(listOf(END))
        }
        val possibleNodes = nodes.getOrDefault(value, emptySet()).filter { !nodesToExclude.contains(it) }
        if (possibleNodes.isEmpty()) {
            return emptyList()
        }
        val smallCave = value != value.uppercase(Locale.getDefault())
        return possibleNodes
            .flatMap { node ->
                findPathsStartingBy(
                    node,
                    if (smallCave) nodesToExclude + value else nodesToExclude
                )
            }
            .filter { it.isNotEmpty() }
            .map {
                listOf(value) + it
            }
    }

    fun findDistinctPathsAllowingTwoVisitsInASmallCave() =
        findPathsAllowingTwoVisitsInASmallCaveStartingBy(START).map { it.joinToString(",") }

    private fun findPathsAllowingTwoVisitsInASmallCaveStartingBy(
        value: String,
        visitedSmallCaves: Map<String, Int> = mapOf()
    ): List<List<String>> {
        if (value == END) {
            return listOf(listOf(END))
        }
        val smallCave = (value != value.uppercase(Locale.getDefault())) && (value != START) && (value != END)
        var nodesToExclude = setOf("start")
        var newVisitedSmallCaves = visitedSmallCaves
        if (smallCave) {
            if (visitedSmallCaves[value] == 2) {
                return emptyList()
            }
            val hasADifferentSmallCaveBeenAlreadyVisitedTwoTimes =
                visitedSmallCaves.filter { it.key != value }.map { it.value }.contains(2)
            if (visitedSmallCaves[value] == 1 && hasADifferentSmallCaveBeenAlreadyVisitedTwoTimes) {
                return emptyList()
            }
            if (hasADifferentSmallCaveBeenAlreadyVisitedTwoTimes) {
                nodesToExclude = visitedSmallCaves.keys.toSet() + "start"
            }
            newVisitedSmallCaves = visitedSmallCaves + Pair(value, visitedSmallCaves.getOrDefault(value, 0) + 1)
        }
        val possibleNodes = nodes.getOrDefault(value, emptySet()).filter { !nodesToExclude.contains(it) }
        if (possibleNodes.isEmpty()) {
            return emptyList()
        }
        return possibleNodes
            .flatMap { node ->
                findPathsAllowingTwoVisitsInASmallCaveStartingBy(
                    node,
                    newVisitedSmallCaves
                )
            }
            .filter { it.isNotEmpty() }
            .map {
                listOf(value) + it
            }
    }
}
