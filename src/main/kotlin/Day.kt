import java.io.InputStream
import java.time.Duration
import java.time.LocalDateTime

abstract class Day<InputCollectionType, Part1OutputType, Part2OutputType> {
    fun run(fileName: String): Pair<Results, Durations>? {
        val start = LocalDateTime.now()
        val dataStream = object {}.javaClass.getResourceAsStream(fileName) ?: return null
        val dataLines = readData(dataStream)
        val afterReadFile = LocalDateTime.now()
        dataStream.close()
        val resultPart1 = runPart1(dataLines)
        val afterPart1 = LocalDateTime.now()
        val resultPart2 = runPart2(dataLines)
        val afterPart2 = LocalDateTime.now()
        return Pair(
            Results(resultPart1.toString(), resultPart2.toString()),
            Durations(
                Duration.between(start, afterReadFile).toFullSeconds(),
                Duration.between(afterReadFile, afterPart1).toFullSeconds(),
                Duration.between(afterPart1, afterPart2).toFullSeconds()
            )
        )
    }

    protected abstract fun readData(data: InputStream): List<InputCollectionType>

    protected abstract fun runPart1(lines: List<InputCollectionType>): Part1OutputType

    protected abstract fun runPart2(lines: List<InputCollectionType>): Part2OutputType
}

fun Duration.toFullSeconds() = this.toString().replace("PT", "").replace("S", "").toDouble()
