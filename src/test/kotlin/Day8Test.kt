import assertk.assertThat
import assertk.assertions.containsOnly
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day8Test {
    @Test
    fun `Should count easy digits`() {
        val lines = object {}.javaClass.getResourceAsStream("day8.txt").bufferedReader().readLines()
        val nbEasyDigits = countEasyDigits(lines)
        assertThat(nbEasyDigits).isEqualTo(26)
    }

    @Nested
    inner class SignalAnalyzerTest {

        private lateinit var signalAnalyzer: SignalAnalyzer

        @BeforeEach
        fun setup() {
            val line = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |cdfeb fcadb cdfeb cdbaf"
            signalAnalyzer = SignalAnalyzer(line)
        }

        @Test
        fun `Should return deductions for stepOne_discoverOne`() {
            val deductions = signalAnalyzer.stepOne_discoverOne()
            assertThat(deductions).containsOnly(
                Deduction(values = mapOf(Segment.TOP_RIGHT to 'a', Segment.BOTTOM_RIGHT to 'b')),
                Deduction(values = mapOf(Segment.TOP_RIGHT to 'b', Segment.BOTTOM_RIGHT to 'a'))
            )
            assertThat(signalAnalyzer.numbers["ab"]).isEqualTo(1)
        }

        @Test
        fun `Should find seven in stepTwo_discoverSeven`() {
            val input = listOf(
                Deduction(values = mapOf(Segment.TOP_RIGHT to 'a', Segment.BOTTOM_RIGHT to 'b')),
                Deduction(values = mapOf(Segment.TOP_RIGHT to 'b', Segment.BOTTOM_RIGHT to 'a'))
            )
            signalAnalyzer.stepOne_discoverOne()
            val deductions = signalAnalyzer.stepTwo_discoverSeven(input)
            assertThat(deductions).containsOnly(
                Deduction(values = mapOf(Segment.TOP to 'd', Segment.TOP_RIGHT to 'a', Segment.BOTTOM_RIGHT to 'b')),
                Deduction(values = mapOf(Segment.TOP to 'd', Segment.TOP_RIGHT to 'b', Segment.BOTTOM_RIGHT to 'a'))
            )
            assertThat(signalAnalyzer.numbers["abd"]).isEqualTo(7)
        }

        @Test
        fun `Should return deductions for stepThree_discoverThree`() {
            val input = listOf(
                Deduction(values = mapOf(Segment.TOP to 'd', Segment.TOP_RIGHT to 'a', Segment.BOTTOM_RIGHT to 'b')),
                Deduction(values = mapOf(Segment.TOP to 'd', Segment.TOP_RIGHT to 'b', Segment.BOTTOM_RIGHT to 'a'))
            )
            val deductionsOne = signalAnalyzer.stepOne_discoverOne()
            signalAnalyzer.stepTwo_discoverSeven(deductionsOne)
            val deductions = signalAnalyzer.stepThree_discoverThree(input)
            assertThat(deductions).containsOnly(
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'c',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'f'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'c',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'f'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                )
            )
            assertThat(signalAnalyzer.numbers["abcdf"]).isEqualTo(3)
        }

        @Test
        fun `Should return deductions for stepFour_discoverZero`() {
            val input = listOf(
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'c',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'f'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'c',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'f'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                )
            )
            val deductionsOne = signalAnalyzer.stepOne_discoverOne()
            val deductionsSeven = signalAnalyzer.stepTwo_discoverSeven(deductionsOne)
            signalAnalyzer.stepThree_discoverThree(deductionsSeven)
            val deductions = signalAnalyzer.stepFour_discoverZero(input)
            assertThat(deductions).containsOnly(
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'g',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'e',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'g',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'e',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                )
            )
            assertThat(signalAnalyzer.numbers["abcdeg"]).isEqualTo(0)
        }

        @Test
        fun `Should return deductions for stepFive_discoverSix`() {
            val input = listOf(
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'g',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'e',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'g',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'e',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                )
            )
            val deductionsOne = signalAnalyzer.stepOne_discoverOne()
            val deductionsSeven = signalAnalyzer.stepTwo_discoverSeven(deductionsOne)
            val deductionsThree = signalAnalyzer.stepThree_discoverThree(deductionsSeven)
            signalAnalyzer.stepFour_discoverZero(deductionsThree)
            signalAnalyzer.stepFive_discoverSix(input)
            assertThat(signalAnalyzer.numbers["bcdefg"]).isEqualTo(6)
        }

        @Test
        fun `Should return deductions for stepSeven_discoverFour`() {
            val input = listOf(
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'g',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'e',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'g',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'e',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                )
            )
            val deductionsOne = signalAnalyzer.stepOne_discoverOne()
            val deductionsSeven = signalAnalyzer.stepTwo_discoverSeven(deductionsOne)
            val deductionsThree = signalAnalyzer.stepThree_discoverThree(deductionsSeven)
            val deductionsZero = signalAnalyzer.stepFour_discoverZero(deductionsThree)
            signalAnalyzer.stepFive_discoverSix(deductionsZero)
            signalAnalyzer.stepSix_discoverNine()
            val deductions = signalAnalyzer.stepSeven_discoverFour(input)
            assertThat(deductions).containsOnly(
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                )
            )
            assertThat(signalAnalyzer.numbers["abef"]).isEqualTo(4)
        }

        @Test
        fun `Should return deductions for stepEight_discoverFive`() {
            val input = listOf(
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'a',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'b',
                        Segment.BOTTOM to 'c'
                    )
                ),
                Deduction(
                    values = mapOf(
                        Segment.TOP to 'd',
                        Segment.TOP_LEFT to 'e',
                        Segment.TOP_RIGHT to 'b',
                        Segment.CENTER to 'f',
                        Segment.BOTTOM_LEFT to 'g',
                        Segment.BOTTOM_RIGHT to 'a',
                        Segment.BOTTOM to 'c'
                    )
                )
            )
            val deductionsOne = signalAnalyzer.stepOne_discoverOne()
            val deductionsSeven = signalAnalyzer.stepTwo_discoverSeven(deductionsOne)
            val deductionsThree = signalAnalyzer.stepThree_discoverThree(deductionsSeven)
            val deductionsZero = signalAnalyzer.stepFour_discoverZero(deductionsThree)
            signalAnalyzer.stepFive_discoverSix(deductionsZero)
            signalAnalyzer.stepSix_discoverNine()
            signalAnalyzer.stepSeven_discoverFour(deductionsZero)
            signalAnalyzer.stepEight_discoverFive(input)
            assertThat(signalAnalyzer.numbers["bcdef"]).isEqualTo(5)
        }

        @Test
        fun `Should analyze signal`() {
            val numbers = signalAnalyzer.analyzeSignal()
            assertThat(numbers["ab"]).isEqualTo(1)
            assertThat(numbers["abd"]).isEqualTo(7)
            assertThat(numbers["abcdf"]).isEqualTo(3)
            assertThat(numbers["abcdeg"]).isEqualTo(0)
            assertThat(numbers["bcdefg"]).isEqualTo(6)
            assertThat(numbers["abcdef"]).isEqualTo(9)
            assertThat(numbers["bcdef"]).isEqualTo(5)
            assertThat(numbers["acdfg"]).isEqualTo(2)
            assertThat(numbers["abef"]).isEqualTo(4)
            assertThat(numbers["abcdefg"]).isEqualTo(8)
        }

        @Test
        fun `Should decode output`() {
            val decodedOutput = signalAnalyzer.decode()
            assertThat(decodedOutput).isEqualTo(5353)
        }

        @Test
        fun `Should decode another line`() {
            val line = "ebc cb aedbf agcef badecg gaebfc bcgf adbcfge ceabf daecgf | cb bce efdab ecbaf"
            signalAnalyzer = SignalAnalyzer(line)
            val decodedOutput = signalAnalyzer.decode()
            assertThat(decodedOutput).isEqualTo(1723)
        }
    }

    @Test
    fun `Should decode list of outputs`() {
        val lines =
            object {}.javaClass.getResourceAsStream("day8.txt").bufferedReader().readLines()
        val outputSum = lines.sumOf {
            val signalAnalyzer = SignalAnalyzer(it)
            signalAnalyzer.decode()
        }
        assertThat(outputSum).isEqualTo(61229)
    }

    @Test
    fun `Should sort String alphabetically`() {
        val chaine = "bdcea"
        assertThat(chaine.sortAlphabetically()).isEqualTo("abcde")
    }
}
