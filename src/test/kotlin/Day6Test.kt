import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test

class Day6Test {

    @Test
    fun `Should pass a day for a single type of fish population`() {
        val population = mapOf(5 to 1L)
        val populationAfterADay = passADayFor(population)
        assertThat(populationAfterADay).isEqualTo(
            mapOf(
                0 to 0L,
                1 to 0L,
                2 to 0L,
                3 to 0L,
                4 to 1L,
                5 to 0L,
                6 to 0L,
                7 to 0L,
                8 to 0L
            )
        )
    }

    @Test
    fun `Should pass a day for a fish having 0 as internal timer`() {
        val population = mapOf(0 to 1L)
        val populationAfterADay = passADayFor(population)
        assertThat(populationAfterADay).isEqualTo(
            mapOf(
                0 to 0L,
                1 to 0L,
                2 to 0L,
                3 to 0L,
                4 to 0L,
                5 to 0L,
                6 to 1L,
                7 to 0L,
                8 to 1L
            )
        )
    }

    @Test
    fun `Should pass a day for a two fishes population`() {
        val population = mapOf(4 to 1, 5 to 1L)
        val populationAfterADay = passADayFor(population)
        assertThat(populationAfterADay).isEqualTo(
            mapOf(
                0 to 0L,
                1 to 0L,
                2 to 0L,
                3 to 1L,
                4 to 1L,
                5 to 0L,
                6 to 0L,
                7 to 0L,
                8 to 0L
            )
        )
    }

    @Test
    fun `Should pass two days for a population`() {
        val population = listOf(5, 4)
        val populationAfterTwoDays = passDaysFor(2, population)
        assertThat(populationAfterTwoDays).isEqualTo(
            mapOf(
                0 to 0L,
                1 to 0L,
                2 to 1L,
                3 to 1L,
                4 to 0L,
                5 to 0L,
                6 to 0L,
                7 to 0L,
                8 to 0L
            )
        )
    }

    @Test
    fun `Should count fishes for a three fishes population`() {
        val population = listOf(5, 4, 0)
        val nbFishes = countFishesAfterDays(2, population)
        assertThat(nbFishes).isEqualTo(4)
    }

    @Test
    fun `Should count fishes for data example`() {
        val population = listOf(3, 4, 3, 1, 2)
        val nbFishes = countFishesAfterDays(18, population)
        assertThat(nbFishes).isEqualTo(26)
    }
}
