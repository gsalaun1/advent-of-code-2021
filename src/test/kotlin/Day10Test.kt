import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day10Test {

    @Nested
    inner class ChunkAnalyzerTest {

        lateinit var chunkAnalyzer: ChunkAnalyzer

        @BeforeEach
        fun setup() {
            val lines = object {}.javaClass.getResourceAsStream("day10.txt").bufferedReader().readLines()
            chunkAnalyzer = ChunkAnalyzer(lines)
        }

        @Nested
        inner class FindFirstIncorrectClosingCharacterIn {
            @Test
            fun `line having } has first incorrect character`() {
                val incorrectCharacter = chunkAnalyzer.findFirstIncorrectClosingCharacterIn("{([(<{}[<>[]}>{[]{[(<()>")
                assertThat(incorrectCharacter).isEqualTo('}')
            }

            @Test
            fun `line having ) has first incorrect character`() {
                val incorrectCharacter = chunkAnalyzer.findFirstIncorrectClosingCharacterIn("[[<[([]))<([[{}[[()]]]")
                assertThat(incorrectCharacter).isEqualTo(')')
            }

            @Test
            fun `line from real file having square bracket has first incorrect character`() {
                val incorrectCharacter =
                    chunkAnalyzer.findFirstIncorrectClosingCharacterIn("[(([{<{(<{{[({{}{}}{[]()})<{{}()}>]}}(([{{{}[]}[[]()]}[<{}[]]{()()}]](({{}{}}{{}()}))){[{({}())[[")
                assertThat(incorrectCharacter).isEqualTo(']')
            }
        }

        @Nested
        inner class SumOfSyntaxErrorScoreBasedOnFirstIllegalCharacter {
            @Test
            fun `Should compute test file`() {
                val score = chunkAnalyzer.sumOfSyntaxErrorScoreBasedOnFirstIllegalCharacter()
                assertThat(score).isEqualTo(26397)
            }
        }

        @Nested
        inner class FindIncompleteLines {
            @Test
            fun `Should search in test file`() {
                val incompleteLines = chunkAnalyzer.findIncompleteLines()
                assertThat(incompleteLines).containsExactlyInAnyOrder(
                    "[({(<(())[]>[[{[]{<()<>>",
                    "[(()[<>])]({[<{<<[]>>(",
                    "(((({<>}<{<{<>}{[]{[]{}",
                    "{<[[]]>}<{[{[{[]{()[[[]",
                    "<{([{{}}[<[[[<>{}]]]>[]]"
                )
            }
        }

        @Nested
        inner class MissingPieceIn {
            @Test
            fun `first incomplete line in test file`() {
                val missingPieces = chunkAnalyzer.missingPieceIn("[({(<(())[]>[[{[]{<()<>>")
                assertThat(missingPieces).isEqualTo("}}]])})]")
            }
        }

        @Nested
        inner class CompletionScoreFor {
            @Test
            fun `last incomplete line in test file`() {
                val completionScore = chunkAnalyzer.completionScoreFor("<{([{{}}[<[[[<>{}]]]>[]]")
                assertThat(completionScore).isEqualTo(294)
            }
        }

        @Nested
        inner class FindMiddleScoreForIncompleteLines {
            @Test
            fun `for test file`() {
                val score = chunkAnalyzer.middleScoreForIncompleteLines()
                assertThat(score).isEqualTo(288957)
            }
        }
    }
}
