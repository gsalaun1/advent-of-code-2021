import OrigamiFolder.Origami
import OrigamiFolder.OrigamiDictionary
import assertk.assertThat
import assertk.assertions.containsAll
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day13Test {

    @Nested
    inner class OrigamiFolderTest {

        private lateinit var origamiFolder: OrigamiFolder

        @BeforeEach
        fun setup() {
            val lines = object {}.javaClass.getResourceAsStream("day13.txt").bufferedReader().readLines()
            origamiFolder = OrigamiFolder(lines)
        }

        @Test
        fun `Should apply all fold instructions`() {
            assertThat(origamiFolder.fold().toString()).isEqualTo(
                """
                #####
                #...#
                #...#
                #...#
                #####
                .....
                .....
                """.trimIndent()
            )
        }

        @Test
        fun `Should count visible points after first fold`() {
            assertThat(origamiFolder.applyFirstFoldInstruction().countVisiblePoints()).isEqualTo(17)
        }

        @Test
        fun `Should contain letters in dictionary`() {
            assertThat(origamiFolder.knownLetters()).containsAll("E", "G", "L", "P", "R", "U")
        }
    }

    @Nested
    inner class OrigamiTest {

        private lateinit var origami: Origami

        private lateinit var dictionary: Map<String, String>

        @BeforeEach
        fun setup() {
            origami = Origami(
                listOf(
                    "6,10",
                    "0,14",
                    "9,10",
                    "0,3",
                    "10,4",
                    "4,11",
                    "6,0",
                    "6,12",
                    "4,1",
                    "0,13",
                    "10,12",
                    "3,4",
                    "3,0",
                    "8,4",
                    "1,10",
                    "2,14",
                    "8,10",
                    "9,0"
                )
            )
            dictionary = mapOf(
                """
                    ####
                    #...
                    ###.
                    #...
                    #...
                    ####
                """.trimIndent()
                    to "E",
                """
                    ###.
                    #..#
                    #..#
                    ###.
                    #...
                    #...
                """.trimIndent()
                    to "P"
            )
        }

        @Test
        fun `Should display points`() {
            assertThat(origami.toString()).isEqualTo(
                """
                ...#..#..#.
                ....#......
                ...........
                #..........
                ...#....#.#
                ...........
                ...........
                ...........
                ...........
                ...........
                .#....#.##.
                ....#......
                ......#...#
                #..........
                #.#........
                """.trimIndent()
            )
        }

        @Test
        fun `Should fold UP`() {
            assertThat(
                origami.fold(
                    OrigamiFolder.FoldInstruction(
                        OrigamiFolder.FoldDirection.UP,
                        7
                    )
                ).toString()
            ).isEqualTo(
                """
                #.##..#..#.
                #...#......
                ......#...#
                #...#......
                .#.#..#.###
                ...........
                ...........
                """.trimIndent()
            )
        }

        @Test
        fun `Should count visible points`() {
            assertThat(origami.countVisiblePoints()).isEqualTo(18)
        }
    }

    @Nested
    inner class OrigamiDictionaryTest {

        private lateinit var origamiDictionary: OrigamiDictionary

        @BeforeEach
        fun setup() {
            origamiDictionary = OrigamiDictionary(
                mapOf(
                    """
                    ####
                    #...
                    ###.
                    #...
                    #...
                    ####
                    """.trimIndent()
                        to "E",
                    """
                    ###.
                    #..#
                    #..#
                    ###.
                    #...
                    #...
                    """.trimIndent()
                        to "P"
                )
            )
        }

        @Test
        fun `Should decrypt one letter word`() {
            assertThat(
                origamiDictionary.decrypt(
                    """
                    ####
                    #...
                    ###.
                    #...
                    #...
                    ####
                    """.trimIndent()
                )
            ).isEqualTo("E")
        }

        @Test
        fun `Should decrypt two letters word`() {
            assertThat(
                origamiDictionary.decrypt(
                    """
                    ####.###.
                    #....#..#
                    ###..#..#
                    #....###.
                    #....#...
                    ####.#...
                    """.trimIndent()
                )
            ).isEqualTo("EP")
        }
    }
}
