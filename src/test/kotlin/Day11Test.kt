import assertk.assertThat
import assertk.assertions.containsOnly
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import Octopuses.Octopus as Octopus

class Day11Test {

    lateinit var octopuses: Octopuses

    @BeforeEach
    fun setup() {
        val lines = object {}.javaClass.getResourceAsStream("day11.txt").bufferedReader().readLines()
        octopuses = Octopuses(lines)
    }

    @Nested
    inner class Instantiante {
        @Test
        fun `from list of lines`() {
            val lines = object {}.javaClass.getResourceAsStream("day11.txt").bufferedReader().readLines()
            octopuses = Octopuses(lines)
            assertThat(octopuses.toString()).isEqualTo(
                """
                5483143223
                2745854711
                5264556173
                6141336146
                6357385478
                4167524645
                2176841721
                6882881134
                4846848554
                5283751526
                """.trimIndent()
            )
        }

        @Test
        fun `from string of lines`() {
            octopuses = Octopuses(
                """
                5483143223
                2745854711
                5264556173
                6141336146
                6357385478
                4167524645
                2176841721
                6882881134
                4846848554
                5283751526
                """.trimIndent()
            )
            assertThat(octopuses.toString()).isEqualTo(
                """
                5483143223
                2745854711
                5264556173
                6141336146
                6357385478
                4167524645
                2176841721
                6882881134
                4846848554
                5283751526
                """.trimIndent()
            )
        }
    }

    @Nested
    inner class GenerateNextStep {

        @Test
        fun `for simple grid with no flashing`() {
            octopuses = Octopuses(
                """
                34543
                40004
                50005
                40004
                34543
                """.trimIndent()
            )
            val nextGrid = octopuses.generateNextStep()
            assertThat(nextGrid.toString()).isEqualTo(
                """
                45654
                51115
                61116
                51115
                45654
                """.trimIndent()
            )
        }

        @Test
        fun `for simple grid with multiple flashings`() {
            octopuses = Octopuses(
                listOf(
                    "11111",
                    "19991",
                    "19191",
                    "19991",
                    "11111"
                )
            )
            val nextGrid = octopuses.generateNextStep()
            assertThat(nextGrid.toString()).isEqualTo(
                """
                34543
                40004
                50005
                40004
                34543
                """.trimIndent()
            )
        }

        @Test
        fun `For large grid`() {
            octopuses = Octopuses(
                """
                5483143223
                2745854711
                5264556173
                6141336146
                6357385478
                4167524645
                2176841721
                6882881134
                4846848554
                5283751526
                """.trimIndent()
            )
            val nextGrid = octopuses.generateNextStep()
            assertThat(nextGrid.toString()).isEqualTo(
                """
                6594254334
                3856965822
                6375667284
                7252447257
                7468496589
                5278635756
                3287952832
                7993992245
                5957959665
                6394862637
                """.trimIndent()
            )
        }

        @Test
        fun `When flashing octopuses generate other flashing octopuses increasing energy of surrounding octopuses`() {
            octopuses = Octopuses(
                """
                6594254334
                3856965822
                6375667284
                7252447257
                7468496589
                5278635756
                3287952832
                7993992245
                5957959665
                6394862637
                """.trimIndent()
            )
            val nextGrid = octopuses.generateNextStep()
            assertThat(nextGrid.toString()).isEqualTo(
                """
                8807476555
                5089087054
                8597889608
                8485769600
                8700908800
                6600088989
                6800005943
                0000007456
                9000000876
                8700006848
                """.trimIndent()
            )
        }
    }

    @Nested
    inner class GenerateNextSteps {
        @Test
        fun `for one step`() {
            val expectedGrid = octopuses.generateNextSteps(1)
            assertThat(expectedGrid.toString()).isEqualTo(
                """
                6594254334
                3856965822
                6375667284
                7252447257
                7468496589
                5278635756
                3287952832
                7993992245
                5957959665
                6394862637
                """.trimIndent()
            )
        }

        @Test
        fun `for two steps`() {
            val expectedGrid = octopuses.generateNextSteps(2)
            assertThat(expectedGrid.toString()).isEqualTo(
                """
                8807476555
                5089087054
                8597889608
                8485769600
                8700908800
                6600088989
                6800005943
                0000007456
                9000000876
                8700006848
                """.trimIndent()
            )
        }

        @Test
        fun `for ten steps`() {
            val expectedGrid = octopuses.generateNextSteps(10)
            assertThat(expectedGrid.toString()).isEqualTo(
                """
                0481112976
                0031112009
                0041112504
                0081111406
                0099111306
                0093511233
                0442361130
                5532252350
                0532250600
                0032240000
                """.trimIndent()
            )
        }

        @Test
        fun `for an hundred steps`() {
            val expectedGrid = octopuses.generateNextSteps(100)
            assertThat(expectedGrid.toString()).isEqualTo(
                """
                0397666866
                0749766918
                0053976933
                0004297822
                0004229892
                0053222877
                0532222966
                9322228966
                7922286866
                6789998766
                """.trimIndent()
            )
        }
    }

    @Nested
    inner class NeighborsOf {
        @Test
        fun `top left Octopus`() {
            val octopus = Octopus(0, 0, 5)
            val neighbors = octopuses.neighborsOf(octopus)
            assertThat(neighbors).containsOnly(
                Octopus(1, 0, 4),
                Octopus(0, 1, 2),
                Octopus(1, 1, 7)
            )
        }

        @Test
        fun `octopus in the grid`() {
            val octopus = Octopus(1, 1, 7)
            val neighbors = octopuses.neighborsOf(octopus)
            assertThat(neighbors).containsOnly(
                Octopus(0, 0, 5),
                Octopus(1, 0, 4),
                Octopus(2, 0, 8),
                Octopus(0, 1, 2),
                Octopus(2, 1, 4),
                Octopus(0, 2, 5),
                Octopus(1, 2, 2),
                Octopus(2, 2, 6)
            )
        }
    }

    @Nested
    inner class CountFlashingOctopusesAfterSteps {
        @Test
        fun `for one step`() {
            val flashingOctopuses = octopuses.countFlashingOctopusesAfterSteps(1)
            assertThat(flashingOctopuses).isEqualTo(0)
        }

        @Test
        fun `for ten steps`() {
            val flashingOctopuses = octopuses.countFlashingOctopusesAfterSteps(10)
            assertThat(flashingOctopuses).isEqualTo(204)
        }
    }

    @Nested
    inner class FindFirstSynchronizingStep {
        @Test
        fun `for example file`() {
            val firstSynchronizingStep = octopuses.findFirstSynchronizingStep()
            assertThat(firstSynchronizingStep).isEqualTo(195)
        }
    }
}
