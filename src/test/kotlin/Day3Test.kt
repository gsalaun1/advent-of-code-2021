import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day3Test {

    @Test
    fun `Should compute line without elements`() {
        val line = emptyList<Int>()
        val computed = computeColumn(line)
        assertThat(computed).isEqualTo(Pair(0, 0))
    }

    @Test
    fun `Should compute line with one element`() {
        val line = listOf(0)
        val computed = computeColumn(line)
        assertThat(computed).isEqualTo(Pair(1, 0))
    }

    @Test
    fun `Should compute line with multiple element`() {
        val line = listOf(0, 1, 0, 1)
        val computed = computeColumn(line)
        assertThat(computed).isEqualTo(Pair(2, 2))
    }

    @Test
    fun `Should compute min max for single element list`() {
        val bits = listOf(Pair(4, 5))
        val minMax = bitsToMinMax(bits)
        assertThat(minMax).isEqualTo(Pair("0", "1"))
    }

    @Test
    fun `Should compute min max for multiple elements list`() {
        val bits = listOf(Pair(4, 5), Pair(5, 6))
        val minMax = bitsToMinMax(bits)
        assertThat(minMax).isEqualTo(Pair("00", "11"))
    }

    @Test
    fun `Should translate binary single character string to base ten`() {
        val binary = "1"
        assertThat(binary.toBaseTen()).isEqualTo(1)
    }

    @Test
    fun `Should translate binary string to base ten`() {
        val binary = "10"
        assertThat(binary.toBaseTen()).isEqualTo(2)
    }

    @Nested
    inner class FilterMostPresent {

        @Test
        fun `Should filter one element list`() {
            val list = listOf("010101")
            val filteredList = list.filterMostPresent()
            assertThat(filteredList).containsExactly("010101")
        }

        @Test
        fun `Should filter two elements list`() {
            val list = listOf("010101", "101010")
            val filteredList = list.filterMostPresent()
            assertThat(filteredList).containsExactly("101010")
        }

        @Test
        fun `Should filter three elements list`() {
            val list = listOf("010101", "101010", "111111")
            val filteredList = list.filterMostPresent()
            assertThat(filteredList).containsExactly("111111")
        }
    }

    @Nested
    inner class FilterMostPresentAtIndex {

        @Test
        fun `Should filter list based on the first character`() {
            val list = listOf("010101", "010101", "101010")
            val filteredList = list.filterMostPresentAtIndex(0)
            assertThat(filteredList).containsExactlyInAnyOrder("010101", "010101")
        }

        @Test
        fun `Should filter list based on the first character, retaining 1 if both characters have the same population`() {
            val list = listOf("010101", "010101", "101010", "101010")
            val filteredList = list.filterMostPresentAtIndex(0)
            assertThat(filteredList).containsExactlyInAnyOrder("101010", "101010")
        }

        @Test
        fun `Should filter list based on the second character, retaining 1 if both characters have the same population`() {
            val list = listOf("010101", "010101", "101010", "101010")
            val filteredList = list.filterMostPresentAtIndex(1)
            assertThat(filteredList).containsExactlyInAnyOrder("010101", "010101")
        }
    }

    @Nested
    inner class SortColumnByBitForIndex {

        @Test
        fun `Should sort column for Index 0`() {
            val list = listOf("010101", "010101", "101010", "101010")
            val sortedColumn = list.sortColumnByBitForIndex(0)
            assertThat(sortedColumn["0"]!!).containsExactlyInAnyOrder("010101", "010101")
            assertThat(sortedColumn["1"]!!).containsExactlyInAnyOrder("101010", "101010")
        }

        @Test
        fun `Should sort column for Index 1`() {
            val list = listOf("010101", "010101", "101010", "101010")
            val sortedColumn = list.sortColumnByBitForIndex(1)
            assertThat(sortedColumn["0"]!!).containsExactlyInAnyOrder("101010", "101010")
            assertThat(sortedColumn["1"]!!).containsExactlyInAnyOrder("010101", "010101")
        }
    }

    // ///////////////////////

    @Nested
    inner class FilterLeastPresent {

        @Test
        fun `Should filter one element list`() {
            val list = listOf("010101")
            val filteredList = list.filterLeastPresent()
            assertThat(filteredList).containsExactly("010101")
        }

        @Test
        fun `Should filter two elements list`() {
            val list = listOf("010101", "101010")
            val filteredList = list.filterLeastPresent()
            assertThat(filteredList).containsExactly("010101")
        }

        @Test
        fun `Should filter three elements list`() {
            val list = listOf("010101", "000000", "101010")
            val filteredList = list.filterLeastPresent()
            assertThat(filteredList).containsExactly("101010")
        }
    }

    @Nested
    inner class FilterLeastPresentAtIndex {

        @Test
        fun `Should filter list based on the first character`() {
            val list = listOf("010101", "010101", "101010")
            val filteredList = list.filterLeastPresentAtIndex(0)
            assertThat(filteredList).containsExactlyInAnyOrder("101010")
        }

        @Test
        fun `Should filter list based on the first character, retaining 0 if both characters have the same population`() {
            val list = listOf("010101", "010101", "101010", "101010")
            val filteredList = list.filterLeastPresentAtIndex(0)
            assertThat(filteredList).containsExactlyInAnyOrder("010101", "010101")
        }

        @Test
        fun `Should filter list based on the second character, retaining 1 if both characters have the same population`() {
            val list = listOf("010101", "010101", "101010", "101010")
            val filteredList = list.filterLeastPresentAtIndex(1)
            assertThat(filteredList).containsExactlyInAnyOrder("101010", "101010")
        }
    }
}
