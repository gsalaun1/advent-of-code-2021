import assertk.assertAll
import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day15Test {

    @Nested
    inner class CavernChitonAvoiderTest {

        @Nested
        inner class FindBestPath {

            @Test
            fun `For 2x2 square`() {
                val chitonAvoider = CavernChitonAvoider(
                    listOf("11", "22")
                )
                assertThat(chitonAvoider.findBestPath()).isEqualTo(3)
            }

            @Test
            fun `For example data`() {
                val lines = object {}.javaClass.getResourceAsStream("day15.txt").bufferedReader().readLines()
                val chitonAvoider = CavernChitonAvoider(lines)
                assertThat(chitonAvoider.findBestPath()).isEqualTo(40)
            }
        }

        @Nested
        inner class BuildWeightedRiskGrid {
            @Test
            fun `Should init all points except first one to max value`() {
                val chitonAvoider = CavernChitonAvoider(
                    listOf("12", "34")
                )
                val weightedRiskPoints = chitonAvoider.points
                assertAll {
                    assertThat(weightedRiskPoints[0][0]).isEqualTo(WeightedRiskPoint(1, 1))
                    assertThat(weightedRiskPoints[1][0]).isEqualTo(WeightedRiskPoint(2, Int.MAX_VALUE))
                    assertThat(weightedRiskPoints[0][1]).isEqualTo(WeightedRiskPoint(3, Int.MAX_VALUE))
                    assertThat(weightedRiskPoints[1][1]).isEqualTo(WeightedRiskPoint(4, Int.MAX_VALUE))
                }
            }
        }

        @Nested
        inner class NotComputedNeighbors {
            @Test
            fun `For 2x2 grid`() {
                val chitonAvoider = CavernChitonAvoider(
                    listOf("12", "34")
                )
                val neighbours = chitonAvoider.notComputedNeighbors(0, 0)
                assertThat(neighbours).containsExactly(
                    Pair(1, 0),
                    Pair(0, 1)
                )
            }

            @Test
            fun `For center point in 3x3 grid`() {
                val chitonAvoider = CavernChitonAvoider(
                    listOf("111", "111", "111")
                )
                val neighbors = chitonAvoider.notComputedNeighbors(1, 1)
                assertThat(neighbors).containsExactly(
                    Pair(0, 1),
                    Pair(2, 1),
                    Pair(1, 0),
                    Pair(1, 2)
                )
            }
        }

        @Nested
        inner class DijkstraStep {
            @Test
            fun `Called one time starting from the top left`() {
                val chitonAvoider = CavernChitonAvoider(
                    listOf("12", "34")
                )
                chitonAvoider.dijkstraStep()
                assertAll {
                    assertThat(chitonAvoider.points[0][0]).isEqualTo(WeightedRiskPoint(1, 1, true))
                    assertThat(chitonAvoider.points[1][0]).isEqualTo(WeightedRiskPoint(2, 3, false))
                    assertThat(chitonAvoider.points[0][1]).isEqualTo(WeightedRiskPoint(3, 4, false))
                    assertThat(chitonAvoider.points[1][1]).isEqualTo(WeightedRiskPoint(4, Int.MAX_VALUE, false))
                }
            }

            @Test
            fun `Called two times starting from the top left`() {
                val chitonAvoider = CavernChitonAvoider(
                    listOf("12", "34")
                )
                chitonAvoider.dijkstraStep()
                chitonAvoider.dijkstraStep()
                assertAll {
                    assertThat(chitonAvoider.points[0][0]).isEqualTo(WeightedRiskPoint(1, 1, true))
                    assertThat(chitonAvoider.points[1][0]).isEqualTo(WeightedRiskPoint(2, 3, true))
                    assertThat(chitonAvoider.points[0][1]).isEqualTo(WeightedRiskPoint(3, 4, false))
                    assertThat(chitonAvoider.points[1][1]).isEqualTo(WeightedRiskPoint(4, 7, false))
                }
            }

            @Test
            fun `Called three times starting from the top left`() {
                val chitonAvoider = CavernChitonAvoider(
                    listOf("12", "34")
                )
                chitonAvoider.dijkstraStep()
                chitonAvoider.dijkstraStep()
                chitonAvoider.dijkstraStep()
                assertAll {
                    assertThat(chitonAvoider.points[0][0]).isEqualTo(WeightedRiskPoint(1, 1, true))
                    assertThat(chitonAvoider.points[1][0]).isEqualTo(WeightedRiskPoint(2, 3, true))
                    assertThat(chitonAvoider.points[0][1]).isEqualTo(WeightedRiskPoint(3, 4, true))
                    assertThat(chitonAvoider.points[1][1]).isEqualTo(WeightedRiskPoint(4, 7, false))
                }
            }

            @Test
            fun `Called one time starting from the top left for 3x3 grid`() {
                val chitonAvoider = CavernChitonAvoider(
                    listOf("123", "456", "789")
                )
                chitonAvoider.dijkstraStep()
                assertAll {
                    assertThat(chitonAvoider.points[0][0]).isEqualTo(WeightedRiskPoint(1, 1, true))
                    assertThat(chitonAvoider.points[1][0]).isEqualTo(WeightedRiskPoint(2, 3, false))
                    assertThat(chitonAvoider.points[2][0]).isEqualTo(WeightedRiskPoint(3, Int.MAX_VALUE, false))
                    assertThat(chitonAvoider.points[0][1]).isEqualTo(WeightedRiskPoint(4, 5, false))
                    assertThat(chitonAvoider.points[1][1]).isEqualTo(WeightedRiskPoint(5, Int.MAX_VALUE, false))
                    assertThat(chitonAvoider.points[2][1]).isEqualTo(WeightedRiskPoint(6, Int.MAX_VALUE, false))
                    assertThat(chitonAvoider.points[0][2]).isEqualTo(WeightedRiskPoint(7, Int.MAX_VALUE, false))
                    assertThat(chitonAvoider.points[1][2]).isEqualTo(WeightedRiskPoint(8, Int.MAX_VALUE, false))
                    assertThat(chitonAvoider.points[2][2]).isEqualTo(WeightedRiskPoint(9, Int.MAX_VALUE, false))
                }
            }

            @Test
            fun `Called three times starting from the top left for 3x3 grid`() {
                val chitonAvoider = CavernChitonAvoider(
                    listOf("123", "456", "789")
                )
                chitonAvoider.dijkstraStep()
                chitonAvoider.dijkstraStep()
                chitonAvoider.dijkstraStep()
                assertAll {
                    assertThat(chitonAvoider.points[0][0]).isEqualTo(WeightedRiskPoint(1, 1, true))
                    assertThat(chitonAvoider.points[1][0]).isEqualTo(WeightedRiskPoint(2, 3, true))
                    assertThat(chitonAvoider.points[2][0]).isEqualTo(WeightedRiskPoint(3, 6, false))
                    assertThat(chitonAvoider.points[0][1]).isEqualTo(WeightedRiskPoint(4, 5, true))
                    assertThat(chitonAvoider.points[1][1]).isEqualTo(WeightedRiskPoint(5, 8, false))
                    assertThat(chitonAvoider.points[2][1]).isEqualTo(WeightedRiskPoint(6, Int.MAX_VALUE, false))
                    assertThat(chitonAvoider.points[0][2]).isEqualTo(WeightedRiskPoint(7, 12, false))
                    assertThat(chitonAvoider.points[1][2]).isEqualTo(WeightedRiskPoint(8, Int.MAX_VALUE, false))
                    assertThat(chitonAvoider.points[2][2]).isEqualTo(WeightedRiskPoint(9, Int.MAX_VALUE, false))
                }
            }
        }
    }

    @Nested
    inner class CaveChitonAvoiderTest {

        @Nested
        inner class ToString {

            @Test
            fun `For 1x1 grid`() {
                val chitonAvoider = CaveChitonAvoider(listOf("1"))
                assertThat(chitonAvoider.toString()).isEqualTo(
                    """
                12345
                23456
                34567
                45678
                56789
                    """.trimIndent()
                )
            }

            @Test
            fun `For 2x2 grid without wrapping over 9`() {
                val chitonAvoider = CaveChitonAvoider(listOf("11", "11"))
                assertThat(chitonAvoider.toString()).isEqualTo(
                    """
                1122334455
                1122334455
                2233445566
                2233445566
                3344556677
                3344556677
                4455667788
                4455667788
                5566778899
                5566778899
                    """.trimIndent()
                )
            }

            @Test
            fun `For 2x2 grid with wrapping over 9`() {
                val chitonAvoider = CaveChitonAvoider(listOf("22", "22"))
                assertThat(chitonAvoider.toString()).isEqualTo(
                    """
                2233445566
                2233445566
                3344556677
                3344556677
                4455667788
                4455667788
                5566778899
                5566778899
                6677889911
                6677889911
                    """.trimIndent()
                )
            }

            @Test
            fun `For Example data`() {
                val lines = object {}.javaClass.getResourceAsStream("day15.txt").bufferedReader().readLines()
                val chitonAvoider = CaveChitonAvoider(lines)
                assertThat(chitonAvoider.toString()).isEqualTo(
                    """
                11637517422274862853338597396444961841755517295286
                13813736722492484783351359589446246169155735727126
                21365113283247622439435873354154698446526571955763
                36949315694715142671582625378269373648937148475914
                74634171118574528222968563933317967414442817852555
                13191281372421239248353234135946434524615754563572
                13599124212461123532357223464346833457545794456865
                31254216394236532741534764385264587549637569865174
                12931385212314249632342535174345364628545647573965
                23119445813422155692453326671356443778246755488935
                22748628533385973964449618417555172952866628316397
                24924847833513595894462461691557357271266846838237
                32476224394358733541546984465265719557637682166874
                47151426715826253782693736489371484759148259586125
                85745282229685639333179674144428178525553928963666
                24212392483532341359464345246157545635726865674683
                24611235323572234643468334575457944568656815567976
                42365327415347643852645875496375698651748671976285
                23142496323425351743453646285456475739656758684176
                34221556924533266713564437782467554889357866599146
                33859739644496184175551729528666283163977739427418
                35135958944624616915573572712668468382377957949348
                43587335415469844652657195576376821668748793277985
                58262537826937364893714847591482595861259361697236
                96856393331796741444281785255539289636664139174777
                35323413594643452461575456357268656746837976785794
                35722346434683345754579445686568155679767926678187
                53476438526458754963756986517486719762859782187396
                34253517434536462854564757396567586841767869795287
                45332667135644377824675548893578665991468977611257
                44961841755517295286662831639777394274188841538529
                46246169155735727126684683823779579493488168151459
                54698446526571955763768216687487932779859814388196
                69373648937148475914825958612593616972361472718347
                17967414442817852555392896366641391747775241285888
                46434524615754563572686567468379767857948187896815
                46833457545794456865681556797679266781878137789298
                64587549637569865174867197628597821873961893298417
                45364628545647573965675868417678697952878971816398
                56443778246755488935786659914689776112579188722368
                55172952866628316397773942741888415385299952649631
                57357271266846838237795794934881681514599279262561
                65719557637682166874879327798598143881961925499217
                71484759148259586125936169723614727183472583829458
                28178525553928963666413917477752412858886352396999
                57545635726865674683797678579481878968159298917926
                57944568656815567976792667818781377892989248891319
                75698651748671976285978218739618932984172914319528
                56475739656758684176786979528789718163989182927419
                67554889357866599146897761125791887223681299833479
                    """.trimIndent()
                )
            }
        }

        @Nested
        inner class FindBestPath {

            @Test
            fun `For example data`() {
                val lines = object {}.javaClass.getResourceAsStream("day15.txt").bufferedReader().readLines()
                val chitonAvoider = CaveChitonAvoider(lines)
                assertThat(chitonAvoider.findBestPath()).isEqualTo(315)
            }
        }
    }
}
