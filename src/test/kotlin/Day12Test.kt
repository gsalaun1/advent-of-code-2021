import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day12Test {

    lateinit var pathFinder: PathFinder

    @Nested
    inner class FindDistinctPaths {
        @Test
        fun `for simple example`() {
            val lines = object {}.javaClass.getResourceAsStream("day12-1.txt").bufferedReader().readLines()
            pathFinder = PathFinder(lines)
            assertThat(pathFinder.findDistinctPaths()).containsExactlyInAnyOrder(
                "start,A,b,A,c,A,end",
                "start,A,b,A,end",
                "start,A,b,end",
                "start,A,c,A,b,A,end",
                "start,A,c,A,b,end",
                "start,A,c,A,end",
                "start,A,end",
                "start,b,A,c,A,end",
                "start,b,A,end",
                "start,b,end"
            )
        }

        @Test
        fun `for middle complexity example`() {
            val lines = object {}.javaClass.getResourceAsStream("day12-2.txt").bufferedReader().readLines()
            pathFinder = PathFinder(lines)
            assertThat(pathFinder.findDistinctPaths()).containsExactlyInAnyOrder(
                "start,HN,dc,HN,end",
                "start,HN,dc,HN,kj,HN,end",
                "start,HN,dc,end",
                "start,HN,dc,kj,HN,end",
                "start,HN,end",
                "start,HN,kj,HN,dc,HN,end",
                "start,HN,kj,HN,dc,end",
                "start,HN,kj,HN,end",
                "start,HN,kj,dc,HN,end",
                "start,HN,kj,dc,end",
                "start,dc,HN,end",
                "start,dc,HN,kj,HN,end",
                "start,dc,end",
                "start,dc,kj,HN,end",
                "start,kj,HN,dc,HN,end",
                "start,kj,HN,dc,end",
                "start,kj,HN,end",
                "start,kj,dc,HN,end",
                "start,kj,dc,end"
            )
        }

        @Test
        fun `for most complex example`() {
            val lines = object {}.javaClass.getResourceAsStream("day12-3.txt").bufferedReader().readLines()
            pathFinder = PathFinder(lines)
            assertThat(pathFinder.findDistinctPaths().size).isEqualTo(226)
        }

        @Test
        fun `for path with no caves`() {
            pathFinder = PathFinder(listOf("start-end"))
            assertThat(pathFinder.findDistinctPaths()).containsExactly("start,end")
        }

        @Test
        fun `for path with a big cave`() {
            pathFinder = PathFinder(listOf("start-A", "A-end"))
            assertThat(pathFinder.findDistinctPaths()).containsExactly("start,A,end")
        }

        @Test
        fun `for path with a small cave`() {
            pathFinder = PathFinder(listOf("start-b", "b-end"))
            assertThat(pathFinder.findDistinctPaths()).containsExactly("start,b,end")
        }

        @Test
        fun `for path with a big and a small cave`() {
            pathFinder = PathFinder(listOf("start-A", "A-b", "b-end"))
            assertThat(pathFinder.findDistinctPaths()).containsExactly("start,A,b,end")
        }
    }

    @Nested
    inner class FindDistinctPathsAllowingTwoVisitsInASmallCave {

        @Test
        fun `for two possible paths`() {
            pathFinder = PathFinder(
                listOf(
                    "start-a",
                    "a-b",
                    "a-end"
                )
            )
            assertThat(pathFinder.findDistinctPathsAllowingTwoVisitsInASmallCave()).containsExactlyInAnyOrder(
                "start,a,end",
                "start,a,b,a,end"
            )
        }

        @Test
        fun `for simple example`() {
            val lines = object {}.javaClass.getResourceAsStream("day12-1.txt").bufferedReader().readLines()
            pathFinder = PathFinder(lines)
            assertThat(pathFinder.findDistinctPathsAllowingTwoVisitsInASmallCave()).containsExactlyInAnyOrder(
                "start,A,b,A,b,A,c,A,end",
                "start,A,b,A,b,A,end",
                "start,A,b,A,b,end",
                "start,A,b,A,c,A,b,A,end",
                "start,A,b,A,c,A,b,end",
                "start,A,b,A,c,A,c,A,end",
                "start,A,b,A,c,A,end",
                "start,A,b,A,end",
                "start,A,b,d,b,A,c,A,end",
                "start,A,b,d,b,A,end",
                "start,A,b,d,b,end",
                "start,A,b,end",
                "start,A,c,A,b,A,b,A,end",
                "start,A,c,A,b,A,b,end",
                "start,A,c,A,b,A,c,A,end",
                "start,A,c,A,b,A,end",
                "start,A,c,A,b,d,b,A,end",
                "start,A,c,A,b,d,b,end",
                "start,A,c,A,b,end",
                "start,A,c,A,c,A,b,A,end",
                "start,A,c,A,c,A,b,end",
                "start,A,c,A,c,A,end",
                "start,A,c,A,end",
                "start,A,end",
                "start,b,A,b,A,c,A,end",
                "start,b,A,b,A,end",
                "start,b,A,b,end",
                "start,b,A,c,A,b,A,end",
                "start,b,A,c,A,b,end",
                "start,b,A,c,A,c,A,end",
                "start,b,A,c,A,end",
                "start,b,A,end",
                "start,b,d,b,A,c,A,end",
                "start,b,d,b,A,end",
                "start,b,d,b,end",
                "start,b,end"
            )
        }

        @Test
        fun `for middle complexity example`() {
            val lines = object {}.javaClass.getResourceAsStream("day12-2.txt").bufferedReader().readLines()
            pathFinder = PathFinder(lines)
            assertThat(pathFinder.findDistinctPathsAllowingTwoVisitsInASmallCave().size).isEqualTo(103)
        }

        @Test
        fun `for most complex example`() {
            val lines = object {}.javaClass.getResourceAsStream("day12-3.txt").bufferedReader().readLines()
            pathFinder = PathFinder(lines)
            assertThat(pathFinder.findDistinctPathsAllowingTwoVisitsInASmallCave().size).isEqualTo(3509)
        }
    }
}
