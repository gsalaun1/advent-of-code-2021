import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day7Test {

    @Nested
    inner class FindBestPositionFor {
        @Test
        fun `Should determine optimized position for one crab`() {
            val crabs = listOf(1)
            val optimizedPosition = findBestPositionFor(crabs)
            assertThat(optimizedPosition).isEqualTo(Pair(1, 0))
        }

        @Test
        fun `Should determine optimized position for two crabs at different positions`() {
            val crabs = listOf(2, 4)
            val optimizedPosition = findBestPositionFor(crabs)
            assertThat(optimizedPosition).isEqualTo(Pair(2, 2))
        }

        @Test
        fun `Should determine optimized position for example`() {
            val crabs = listOf(16, 1, 2, 0, 4, 2, 7, 1, 2, 14)
            val optimizedPosition = findBestPositionFor(crabs)
            assertThat(optimizedPosition).isEqualTo(Pair(2, 37))
        }
    }

    @Nested
    inner class FindBestPositionForIncremental {
        @Test
        fun `Should determine optimized position for one crab`() {
            val crabs = listOf(1)
            val optimizedPosition = findBestPositionForIncremental(crabs)
            assertThat(optimizedPosition).isEqualTo(Pair(1, 0))
        }

        @Test
        fun `Should determine optimized position for two crabs at different positions`() {
            val crabs = listOf(2, 4)
            val optimizedPosition = findBestPositionForIncremental(crabs)
            assertThat(optimizedPosition).isEqualTo(Pair(3, 2))
        }

        @Test
        fun `Should determine optimized position for example`() {
            val crabs = listOf(16, 1, 2, 0, 4, 2, 7, 1, 2, 14)
            val optimizedPosition = findBestPositionForIncremental(crabs)
            assertThat(optimizedPosition).isEqualTo(Pair(5, 168))
        }
    }
}
