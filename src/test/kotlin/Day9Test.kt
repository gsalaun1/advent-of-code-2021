import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.containsOnly
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day9Test {

    @Nested
    inner class CountRiskLevelsSum {

        @Test
        fun `Should count risk levels sum in sample file`() {
            val lines = object {}.javaClass.getResourceAsStream("day9.txt").bufferedReader().readLines()
            val smokeMatrix = SmokeMatrix(lines)
            val riskLevelsSum = smokeMatrix.countRiskLevelsSum()
            assertThat(riskLevelsSum).isEqualTo(15)
        }
    }

    @Nested
    inner class GetLowPoints {
        @Test
        fun `Should return low points in middle of the matrix`() {
            val lines = listOf("333", "323", "333")
            val smokeMatrix = SmokeMatrix(lines)
            val lowPoints = smokeMatrix.getLowPoints()
            assertThat(lowPoints.map { it.value }).containsOnly(2)
        }
    }

    @Nested
    inner class FindNeighborsOf {

        private lateinit var smokeMatrix: SmokeMatrix

        @BeforeEach
        fun setup() {
            smokeMatrix = SmokeMatrix(listOf("333", "323", "333"))
        }

        @Test
        fun `top left point`() {
            val topLeftPoint = SmokeMatrix.SmokePoint(0, 0, 3)
            val neighbors = smokeMatrix.findNeighborsOf(topLeftPoint)
            assertThat(neighbors).containsExactly(
                SmokeMatrix.SmokePoint(1, 0, 3),
                SmokeMatrix.SmokePoint(0, 1, 3)
            )
        }

        @Test
        fun `middle point`() {
            val topLeftPoint = SmokeMatrix.SmokePoint(1, 1, 2)
            val neighbors = smokeMatrix.findNeighborsOf(topLeftPoint)
            assertThat(neighbors).containsExactly(
                SmokeMatrix.SmokePoint(1, 0, 3),
                SmokeMatrix.SmokePoint(0, 1, 3),
                SmokeMatrix.SmokePoint(2, 1, 3),
                SmokeMatrix.SmokePoint(1, 2, 3)
            )
        }
    }

    @Nested
    inner class GetBasinPoints {

        @Test
        fun `Should return basin points for a tiny single horizontal basin size`() {
            val smokeMatrix = SmokeMatrix(listOf("989"))
            val lowPoint = smokeMatrix.getLowPoints().first()
            val basinPoints = smokeMatrix.getBasinPoints(lowPoint)
            assertThat(basinPoints).containsOnly(SmokeMatrix.SmokePoint(1, 0, 8))
        }

        @Test
        fun `Should return basin points for a single horizontal basin size`() {
            val smokeMatrix = SmokeMatrix(listOf("9876789"))
            val lowPoint = smokeMatrix.getLowPoints().first()
            val basinPoints = smokeMatrix.getBasinPoints(lowPoint)
            assertThat(basinPoints).containsOnly(
                SmokeMatrix.SmokePoint(1, 0, 8),
                SmokeMatrix.SmokePoint(
                    2, 0, 7
                ),
                SmokeMatrix.SmokePoint(3, 0, 6),
                SmokeMatrix.SmokePoint(4, 0, 7),
                SmokeMatrix.SmokePoint(5, 0, 8),
            )
        }

        @Test
        fun `Should return basin points for two lines`() {
            val smokeMatrix = SmokeMatrix(
                listOf("9876789", "9987899")
            )
            val lowPoint = smokeMatrix.getLowPoints().first()
            val basinPoints = smokeMatrix.getBasinPoints(lowPoint)
            assertThat(basinPoints).containsOnly(
                SmokeMatrix.SmokePoint(1, 0, 8),
                SmokeMatrix.SmokePoint(2, 0, 7),
                SmokeMatrix.SmokePoint(3, 0, 6),
                SmokeMatrix.SmokePoint(4, 0, 7),
                SmokeMatrix.SmokePoint(5, 0, 8),
                SmokeMatrix.SmokePoint(2, 1, 8),
                SmokeMatrix.SmokePoint(3, 1, 7),
                SmokeMatrix.SmokePoint(4, 1, 8)
            )
        }
    }

    @Nested
    inner class GetBasinsSize {

        @Test
        fun `Should return single horizontal basin size`() {
            val smokeMatrix = SmokeMatrix(listOf("9876789"))
            val basinsSize = smokeMatrix.getBasinsSize()
            assertThat(basinsSize).containsExactly(5)
        }

        @Test
        fun `Should return basins size for entire test file`() {
            val lines = object {}.javaClass.getResourceAsStream("day9.txt").bufferedReader().readLines()
            val smokeMatrix = SmokeMatrix(lines)
            val basinsSize = smokeMatrix.getBasinsSize()
            assertThat(basinsSize).containsExactlyInAnyOrder(3, 9, 9, 14)
        }
    }

    @Nested
    inner class MultiplyLargestBasins {
        @Test
        fun `Should compute test file`() {
            val lines = object {}.javaClass.getResourceAsStream("day9.txt").bufferedReader().readLines()
            val smokeMatrix = SmokeMatrix(lines)
            val basinsSize = smokeMatrix.multiplyLargestBasins()
            assertThat(basinsSize).isEqualTo(1134)
        }
    }
}
