import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day5Test {

    @Nested
    inner class VentGridTest {

        private lateinit var grid: VentGrid

        @BeforeEach
        fun setup() {
            grid = VentGrid()
        }

        @Test
        fun `Should build empty grid`() {
            assertThat(grid.toString()).isEqualTo("")
        }

        @Test
        fun `Should build grid with one point`() {
            grid.addVent("0,0 -> 0,0")
            assertThat(grid.toString()).isEqualTo("v")
        }

        @Test
        fun `Should build grid with one horizontally vent on first line`() {
            grid.addVent("0,0 -> 3,0")
            assertThat(grid.toString()).isEqualTo("vvvv")
        }

        @Test
        fun `Should build grid with one horizontally vent on first line not starting at 0`() {
            grid.addVent("1,0 -> 3,0")
            assertThat(grid.toString()).isEqualTo(".vvv")
        }

        @Test
        fun `Should build grid with one horizontally vent on second line`() {
            grid.addVent("0,1 -> 3,1")
            assertThat(grid.toString()).isEqualTo(
                """
                ....
                vvvv
                """.trimIndent()
            )
        }

        @Test
        fun `Should build grid with one horizontally vent from right to left`() {
            grid.addVent("3,0 -> 0,0")
            assertThat(grid.toString()).isEqualTo("vvvv")
        }

        @Test
        fun `Should build grid with one vertically line on first column`() {
            grid.addVent("0,0 -> 0,3")
            assertThat(grid.toString()).isEqualTo(
                """
                v
                v
                v
                v
                """.trimIndent()
            )
        }

        @Test
        fun `Should build grid with one vertically line from bottom to top`() {
            grid.addVent("0,3 -> 0,0")
            assertThat(grid.toString()).isEqualTo(
                """
                v
                v
                v
                v
                """.trimIndent()
            )
        }

        @Test
        fun `Should build grid from multiple lines`() {
            grid.addVent("0,9 -> 5,9")
            grid.addVent("8,0 -> 0,8")
            grid.addVent("9,4 -> 3,4")
            grid.addVent("2,2 -> 2,1")
            grid.addVent("7,0 -> 7,4")
            grid.addVent("6,4 -> 2,0")
            grid.addVent("0,9 -> 2,9")
            grid.addVent("3,4 -> 1,4")
            grid.addVent("0,0 -> 8,8")
            grid.addVent("5,5 -> 8,2")
            assertThat(grid.toString()).isEqualTo(
                """
                .......v..
                ..v....v..
                ..v....v..
                .......v..
                .vvvvvvvvv
                ..........
                ..........
                ..........
                ..........
                vvvvvv....
                """.trimIndent()
            )
        }

        @Test
        fun `Should build grid from file`() {
            val vents = object {}.javaClass.getResourceAsStream("day5.txt").bufferedReader().readLines()
            grid.addVents(vents)
            assertThat(grid.toString()).isEqualTo(
                """
                .......v..
                ..v....v..
                ..v....v..
                .......v..
                .vvvvvvvvv
                ..........
                ..........
                ..........
                ..........
                vvvvvv....
                """.trimIndent()
            )
        }

        @Test
        fun `Should build overlapping report`() {
            val vents = object {}.javaClass.getResourceAsStream("day5.txt").bufferedReader().readLines()
            grid.addVents(vents)
            assertThat(grid.overlappingReport()).isEqualTo(
                """
                .......1..
                ..1....1..
                ..1....1..
                .......1..
                .112111211
                ..........
                ..........
                ..........
                ..........
                222111....
                """.trimIndent()
            )
        }

        @Test
        fun `Should count overlapping based on criteria`() {
            val vents = object {}.javaClass.getResourceAsStream("day5.txt").bufferedReader().readLines()
            grid.addVents(vents)
            assertThat(grid.overlappingCriteriaCount(2)).isEqualTo(5)
        }
    }

    @Nested
    inner class VentGridConsideringDiagonalsTest {

        private lateinit var grid: VentGrid

        @BeforeEach
        fun setup() {
            grid = VentGrid(true)
        }

        @Test
        fun `Should build grid with one diagonal line from top left to bottom right`() {
            grid.addVent("0,0 -> 3,3")
            assertThat(grid.toString()).isEqualTo(
                """
                v...
                .v..
                ..v.
                ...v
                """.trimIndent()
            )
        }

        @Test
        fun `Should build grid with one diagonal line not starting on top left`() {
            grid.addVent("1,1 -> 5,5")
            assertThat(grid.toString()).isEqualTo(
                """
                ......
                .v....
                ..v...
                ...v..
                ....v.
                .....v
                """.trimIndent()
            )
        }

        @Test
        fun `Should build grid with one diagonal line from top right to bottom left`() {
            grid.addVent("5,0 -> 0,5")
            assertThat(grid.toString()).isEqualTo(
                """
                .....v
                ....v.
                ...v..
                ..v...
                .v....
                v.....
                """.trimIndent()
            )
        }

        @Test
        fun `Should build grid with one diagonal line from bottom left to top right`() {
            grid.addVent("0,5 -> 5,0")
            assertThat(grid.toString()).isEqualTo(
                """
                .....v
                ....v.
                ...v..
                ..v...
                .v....
                v.....
                """.trimIndent()
            )
        }

        @Test
        fun `Should build grid with one diagonal line from bottom right to top left`() {
            grid.addVent("5,5 -> 0,0")
            assertThat(grid.toString()).isEqualTo(
                """
                v.....
                .v....
                ..v...
                ...v..
                ....v.
                .....v
                """.trimIndent()
            )
        }

        @Test
        fun `Should build overlapping report`() {
            val vents = object {}.javaClass.getResourceAsStream("day5.txt").bufferedReader().readLines()
            grid.addVents(vents)
            assertThat(grid.overlappingReport()).isEqualTo(
                """
                1.1....11.
                .111...2..
                ..2.1.111.
                ...1.2.2..
                .112313211
                ...1.2....
                ..1...1...
                .1.....1..
                1.......1.
                222111....
                """.trimIndent()
            )
        }

        @Test
        fun `Should count overlapping based on criteria`() {
            val vents = object {}.javaClass.getResourceAsStream("day5.txt").bufferedReader().readLines()
            grid.addVents(vents)
            assertThat(grid.overlappingCriteriaCount(2)).isEqualTo(12)
        }
    }
}
