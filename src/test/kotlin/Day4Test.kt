import assertk.assertThat
import assertk.assertions.contains
import assertk.assertions.containsExactly
import assertk.assertions.isEqualTo
import assertk.assertions.isFalse
import assertk.assertions.isTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day4Test {
    @Nested
    inner class BingoGridClass {

        private lateinit var bingoGrid: BingoGrid

        @BeforeEach
        fun setup() {
            bingoGrid = BingoGrid()
        }

        @Nested
        inner class AddLine {
            @Test
            fun `Should add one line`() {
                bingoGrid.addLine("10 11 12 13 14 15")
                assertThat(bingoGrid.lines()).containsExactly(listOf(10, 11, 12, 13, 14, 15))
            }

            @Test
            fun `Should add one line starting with space`() {
                bingoGrid.addLine(" 10 11 12 13 14 15")
                assertThat(bingoGrid.lines()).containsExactly(listOf(10, 11, 12, 13, 14, 15))
            }

            @Test
            fun `Should add one line containing double spaces`() {
                bingoGrid.addLine("10 11  2 13 14 15")
                assertThat(bingoGrid.lines()).containsExactly(listOf(10, 11, 2, 13, 14, 15))
            }
        }

        @Nested
        inner class IsComplete {
            @Test
            fun `Should be false if empty`() {
                assertThat(bingoGrid.isComplete()).isFalse()
            }

            @Test
            fun `Should be false if line count is different from column count`() {
                bingoGrid.addLine("10 11  2 13 14 15")
                assertThat(bingoGrid.isComplete()).isFalse()
            }

            @Test
            fun `Should be true if line count is equal to column count`() {
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.addLine("1 2 3 4 5")
                assertThat(bingoGrid.isComplete()).isTrue()
            }
        }

        @Nested
        inner class PushToken {
            @Test
            fun `Should push Token`() {
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.pushToken(3)
                assertThat(bingoGrid.toString()).contains("(3)")
            }
        }

        @Nested
        inner class IsAWinner {
            @Test
            fun `Should return false is grid is not a winner`() {
                bingoGrid.addLine("1 2 3 4 5")
                assertThat(bingoGrid.isAWinner()).isFalse()
            }

            @Test
            fun `Should return true if grid is an horizontally winner`() {
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.pushToken(1)
                bingoGrid.pushToken(2)
                bingoGrid.pushToken(3)
                bingoGrid.pushToken(4)
                bingoGrid.pushToken(5)
                assertThat(bingoGrid.isAWinner()).isTrue()
            }

            @Test
            fun `Should return true if grid is a vertically winner on first column`() {
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.addLine("6 7 8 9 10")
                bingoGrid.addLine("11 12 13 15 15")
                bingoGrid.pushToken(1)
                bingoGrid.pushToken(6)
                bingoGrid.pushToken(11)
                assertThat(bingoGrid.isAWinner()).isTrue()
            }

            @Test
            fun `Should return true if grid is a vertically winner on second column`() {
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.addLine("6 7 8 9 10")
                bingoGrid.addLine("11 12 13 15 15")
                bingoGrid.pushToken(2)
                bingoGrid.pushToken(7)
                bingoGrid.pushToken(12)
                assertThat(bingoGrid.isAWinner()).isTrue()
            }
        }

        @Nested
        inner class SumUncheckedCells {
            @Test
            fun `Should return sum of all cells if no token has been pushed`() {
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.addLine("6 7 8 9 10")
                assertThat(bingoGrid.sumUncheckedCells()).isEqualTo(55)
            }

            @Test
            fun `Should not sum checked cells`() {
                bingoGrid.addLine("1 2 3 4 5")
                bingoGrid.addLine("6 7 8 9 10")
                bingoGrid.pushToken(6)
                assertThat(bingoGrid.sumUncheckedCells()).isEqualTo(49)
            }
        }
    }
}
