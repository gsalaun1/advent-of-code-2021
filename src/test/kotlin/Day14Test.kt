import Polymerizer.Template
import assertk.assertThat
import assertk.assertions.containsOnly
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day14Test {

    @Nested
    inner class PolymerizerTest {
        private lateinit var polymerizer: Polymerizer

        @BeforeEach
        fun setup() {
            val lines = object {}.javaClass.getResourceAsStream("day14.txt").bufferedReader().readLines()
            polymerizer = Polymerizer(lines)
        }

        @Test
        fun `Should generate template applying rules one time`() {
            assertThat(polymerizer.applyRules().patterns).isEqualTo(
                mapOf(
                    "NC" to 1L,
                    "CN" to 1L,
                    "NB" to 1L,
                    "BC" to 1L,
                    "CH" to 1L,
                    "HB" to 1L
                )
            )
        }

        @Test
        fun `Find difference between most and least common elements count for 10 iterations`() {
            assertThat(polymerizer.differenceBetweendMostAndLeastCommonElements(10)).isEqualTo(1588)
        }

        @Test
        fun `Find difference between most and least common elements count for 40 iterations`() {
            assertThat(polymerizer.differenceBetweendMostAndLeastCommonElements(40)).isEqualTo(2188189693529)
        }
    }

    @Nested
    inner class TemplateTest {
        private lateinit var template: Template

        private lateinit var rulesDictionary: Map<String, Pair<String, String>>

        @BeforeEach
        fun setup() {
            template = Template("NNCB")
            rulesDictionary = mapOf(
                "NN" to Pair("NC", "CN"),
                "NC" to Pair("NB", "BC"),
                "CB" to Pair("CH", "HB")
            )
        }

        @Test
        fun `Should split template in group of patterns`() {
            assertThat(template.patterns).containsOnly(
                Pair("NN", 1L),
                Pair("NC", 1L),
                Pair("CB", 1L)
            )
        }

        @Test
        fun `Should split template containing doublons in group of patterns`() {
            template = Template("NNNCB")
            assertThat(template.patterns).isEqualTo(
                mapOf(
                    "NN" to 2L,
                    "NC" to 1L,
                    "CB" to 1L
                )
            )
        }

        @Test
        fun `Should generate template applying rules`() {
            assertThat(template.apply(rulesDictionary).patterns).isEqualTo(
                mapOf(
                    "NC" to 1L,
                    "CN" to 1L,
                    "NB" to 1L,
                    "BC" to 1L,
                    "CH" to 1L,
                    "HB" to 1L
                )
            )
        }

        @Test
        fun `Should group chars by Count`() {
            assertThat(template.countChars()).containsOnly(
                Pair('N', 2L),
                Pair('C', 1L),
                Pair('B', 1L)
            )
        }
    }
}
